\chapter{Large Radius Tracking}
\label{sec:app_lrt}

The performance of the large radius tracking was examined by a small analysis team including the author, and this performance was documented in the ATLAS public note~\cite{ATL-PHYS-PUB-2017-014}.

Two models were used to evaluate the performance of the large radius tracking.
In one of the models, a split-SUSY model (shown in Figure~\ref{subfig:displHadSample}), long-lived gluinos decay to a quark and a virtual squark, the latter of which decay to neutralinos and quarks, leading to displaced hadronic jets (\textit{displaced hadrons}).
In the other model (shown in Figure~\ref{subfig:displLeptSample}), a squark decays to a quark and a neutralino, and the neutralino then decays to displaced leptons (\textit{displaced leptons}).
While the displaced lepton model allows for decays to $ee$, $\mu\mu$, and $e\mu$, only decays to $\mu\mu$ were considered for the performance study.
%The the displaced lepton model, while the model allows for decays to $ee$, $\mu\mu$, and $e\mu$, only decays to $\mu\mu$ were considered for the performance study.

\begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:displHadSample}\includegraphics[width=0.5\textwidth]{Appendices/Figures/fig_02b_displacedhardon.pdf}}
	\subfloat[]{\label{subfig:displLeptSample}\includegraphics[width=0.5\textwidth]{Appendices/Figures/fig_02a_displacedeleptons.pdf}} 
 \caption{
The (a) displaced hadron and (b) displaced lepton samples used for the LRT performance evaluation in Ref.~\cite{ATL-PHYS-PUB-2017-014}. 
}
  \label{fig:LRTsamples}
\end{figure}

In addition to the difference in the displaced decay products, the two samples were chosen due to the differences in the kinematic properties of those decay products in order to provide a wide array of signal decays for testing purposes.
The production radius ($r_{\mathrm{prod}}$) and \pt~of the signal particles (decay products which result from the signal processes diagrammed in Figure~\ref{fig:LRTsamples}) are shown in Figure~\ref{fig:LRTkinematics}.
The signal particles in the displaced leptons sample have production radii predominantly between 0 and 200~[mm], which are quite displaced.
The signal particles in the displaced hadrons sample are even more displaced on average, with a range of production radii extending to over 400~mm, which allows the performance studies of the LRT to probe the entire range of the pixel and the first layers of the SCT (Figure~\ref{subfig:LRT_rProd}).
The decay products in the displaced hadrons sample are fairly soft while the decay products in the displaced leptons sample are much harder, allowing the LRT performance to be tested for decay vertices with a wide range of boost (Figure~\ref{subfig:LRT_pT}).

\begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:LRT_rProd}\includegraphics[width=0.5\textwidth]{Appendices/Figures/fig_03c_rprodLRT.pdf}}
	\subfloat[]{\label{subfig:LRT_pT}\includegraphics[width=0.5\textwidth]{Appendices/Figures/fig_03d_pTLRT.pdf}} 
 \caption{
The (a) production radius [mm] and (b) \pt~[GeV] of the signal particles in the displaced hadron and displaced leptons samples used for the LRT performance. 
Plots were created by the author and published in~\cite{ATL-PHYS-PUB-2017-014}.
}
  \label{fig:LRTkinematics}
\end{figure}

The efficiency to reconstruct the displaced hadrons and displaced leptons is shown versus the decay product production radius in Figure~\ref{fig:LRTeff}. 
The efficiency is defined as the fraction of the generated signal particles which are matched to reconstructed tracks.
To be matched to a reconstructed track, the weighted fraction of hits left by the generated particle that are included in the reconstructed track must be $\geq 0.5$.
The hits are weighted according to the sub-detector they are found in to account for the different resolutions of the different sub-detectors.
The generated particles are required to be in the fiducial volume, within $r_{\mathrm{prod}} <$ 440~mm, $|\eta| < 2.5$.
The generated particles are also required to have \pt~$>$ 1~GeV, must be charged, and must be decay products of the signal process.
The reconstruction efficiency is shown for the LRT (blue points) and the standard tracking (red circles) individually, and for the combined track collection (black triangles).

\begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:LRTeff_had}\includegraphics[width=0.5\textwidth]{Appendices/Figures/fig_10b_displHadLRTeff.pdf}}
	\subfloat[]{\label{subfig:LRTeff_lep}\includegraphics[width=0.5\textwidth]{Appendices/Figures/fig_10a_displLepLRTeff.pdf}} 
 \caption{
Reconstruction efficiency of the displaced decay products in the (a) displaced hadrons and (b) displaced leptons signal test samples, as published in~\cite{ATL-PHYS-PUB-2017-014}. 
The blue squares represent the reconstruction efficiency of the decay products using only large radius tracks, the red circles represent the reconstruction efficiency using only standard tracks, and the black triangles use both the standard and large radius tracks. 
}
  \label{fig:LRTeff}
\end{figure}

The generated particles are are primarily reconstructed using the standard tracking for production radii $< 10$-20~mm, after which point the large radius tracking reconstructs the majority of the decay products out to a radius of approximately 200~mm.
The reconstruction efficiency decreases as the production radius increases.
This is due to the reconstruction requirement of $\geq 7$ silicon hits per track.

As shown in Figure~\ref{fig:ID_overview}, particles that are produced after the last layer of the pixel detector and particularly after the first layer of the SCT are unlikely to traverse the necessary number of layers of silicon in order to have at least 7 silicon hits, this will inevitably cause a reduction in the reconstruction efficiency.
To evaluate the reconstruction efficiency of reconstructable particles, a technical efficiency is examined.

In Figure~\ref{fig:LRTtechEff}, the technical efficiency is determined in the same manner as the reconstruction efficiency, except the generated signal particles are required to have left energy deposits on at least 7 silicon layers.
As in Figure~\ref{fig:LRTeff}, the blue squares represent the efficiency using only the large radius tracks, and the black squares represent the technical efficiency taking into account all reconstructed tracks.

\begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:LRTtechEff_had}\includegraphics[width=0.5\textwidth]{Appendices/Figures/fig_11b_displHadTechEff.pdf}}
	\subfloat[]{\label{subfig:LRTtechEff_lep}\includegraphics[width=0.5\textwidth]{Appendices/Figures/fig_11a_displLepTechEff.pdf}} 
 \caption{
The technical efficiency of the displaced decay products in the (a) displaced hadrons and (b) displaced leptons signal test samples.
Plots were created by the author and published in~\cite{ATL-PHYS-PUB-2017-014}.
The blue squares represent the technical efficiency using only large radius tracks and the black triangles represent the technical efficiency using the combined track collection.
}
  \label{fig:LRTtechEff}
\end{figure}

The technical efficiency for reconstructing generated signal particles which leave at least 7 energy deposits on silicon layers is consistently high versus the production radius of the particle.
After a production radius of 20~mm, the signal particles are predominately reconstructed by the large radius tracking, and the total technical efficiency is at least 80\% out to a production radius of 300~mm.
Of the particles that are reconstructable considering the requirement of at least 7 silicon hits, most are reconstructed using the combination of the standard and large radius tracking.
While the reconstruction efficiency is what ultimately is important for analyses concerned with displaced decays, the technical efficiency is important to consider when evaluating the performance of the algorithm.

While the efficiency for reconstructing signal particles is high, the large radius tracking algorithm produces a large quantity of poor-quality tracks (a poor-quality track is a track that is not matched to a generated particle) and consequently has a very high CPU usage.
Due to this, the LRT algorithm can only be run on a small subset of events and was not able to be run at trigger level in Run 2.
Several studies were performed both before and after the publication of the public note~\cite{ATL-PHYS-PUB-2017-014} to determine methods to reduce the rate of poor-quality track reconstruction.

\begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:LRT_SCT_fake}\includegraphics[width=0.5\textwidth]{Appendices/Figures/fig_16_SCT.pdf}}
	\subfloat[]{\label{subfig:LRT_TRT_fake}\includegraphics[width=0.5\textwidth]{Appendices/Figures/fig_15a_TRT.pdf}} 
 \caption{
 The fraction of reconstructed tracks which are poor-quality (open black triangles) compared to the fraction of reconstructed tracks which are matched to generated signal particles (red circles) compared to (a) the number of SCT hits used in the reconstructed track and (b) the number of TRT hits in the TRT extension of the track (if it is successful).
Plots were created by the author and published in~\cite{ATL-PHYS-PUB-2017-014}.
}
  \label{fig:SCT_TRT_fake}
\end{figure}

Figure~\ref{fig:SCT_TRT_fake} shows the fraction of reconstructed tracks which are poor-quality (open black triangles) compared to those which are matched to generated signal particles (red circles) versus the number of SCT hits in the reconstructed tracks (Figure~\ref{subfig:LRT_SCT_fake}) and the number of TRT hits in the TRT extension (Figure~\ref{subfig:LRT_TRT_fake}). 
If the TRT extension fails the number of hits is 0.

Figure~\ref{subfig:LRT_TRT_fake} demonstrates that the fraction of poor-quality tracks would be reduced if the number of TRT hits in a TRT extension was increased (and the TRT extension was required to be successful assuming the tracks are reconstructed in the $|\eta|$ range of the TRT), without significantly decreasing the fraction of reconstructed tracks which are matched to generated signal particles.
Figure~\ref{subfig:LRT_SCT_fake} demonstrates likewise that the fraction of poor-quality tracks would be reduced if the number of SCT hits was increased to 8.
The number of SCT hits becomes an important consideration for highly displaced tracks which may only have one or two reconstructed hits in the pixel detector.
The addition of a minimum SCT hit requirement in addition to a minimum silicon hit requirement requires more careful study due to the increased dependence on the function of the SCT detector.

The average pileup increased throughout Run 2, so the dependence of the LRT reconstruction efficiency on \pu~needed to be considered.
Figure~\ref{fig:LRT_PT_mu} shows the reconstruction efficiency versus \pu~for the reconstruction efficiency using the large radius tracking (open points) and the combined track collection (filled points).
Figure~\ref{fig:LRT_PT_mu} demonstrates that there is a negative correlation between the amount of \pu~and the reconstruction efficiency; this difference is predominately due to the LRT reconstructed tracks.
By increasing the minimum \pt~for the LRT, the dependence on \pu~is slightly mitigated, which has a greater impact for sets of data with \pu~over 60, as was produced in 2018.
The mitigation of the decrease in efficiency is due to the removal of the low \pt~poor-quality tracks which are created, using hits which belong to tracks from decays of interest. 

\begin{figure}[!h] 
	\centering
	  \includegraphics[width=0.7\textwidth]{Appendices/Figures/Eff_mu_pT500v900_thesis.pdf}
	  \caption{
	  The reconstruction efficiency from the large radius tracking (open points) and the combined track collection (filled points) considering a minimum \pt~of 500~MeV (black circles) or 900~MeV (red squares).
	  }
\label{fig:LRT_PT_mu}
\end{figure} 