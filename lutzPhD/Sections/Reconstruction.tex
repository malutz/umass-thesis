\chapter{Reconstruction}
\label{sec:reco}

\section{Standard objects}

\subsection{Jets}
%Hadronic jets are used in the isolation of the reconstructed vertices in the muon spectrometer (described in Section~\ref{sec:recoMSVX}).
In the ATLAS detector, jets are reconstructed from energy deposits in the ECal and the HCal.
Nearby calorimeter cells with energy above a noise threshold are built into three-dimensional topological clusters~\cite{PERF-2016-04}.
The energy of the calorimeter cells is measured at the electromagnetic energy scale, assuming the energy is associated to electrically charged particles.
The jets are reconstructed using the software \textsc{FastJet} 2.4.3~\cite{Fastjet}.
Jets with \pt~$>$ 7 GeV are reconstructed using the anti-$k_t$ algorithm~\cite{Cacciari:2008gp}.

The anti-$k_t$ algorithm is a jet clustering algorithm which clusters particles according to 
\begin{equation}
 \begin{split}
 d_{ij} = \mathrm{min}(k_{ti}^{2p},k_{tj}^{2p})\frac{\Delta_{ij}^2}{\mathrm{R}^2}, \\ \quad
 d_{iB} = k_{ti}^{2p}, \\  \quad
p = -1 
 \label{eq:jetCluster}
\end{split}
 \end{equation}
 in which $d_{ij}$ is the distance between particles and $d_{iB}$ is the distance between a particle and the beam, R is the radius parameter of the jet, and $\Delta_{ij} = (y_i - y_j)^2 + (\phi_i - \phi_j)^2$ ($k_{ti}$ and $y_i$ are the transverse momentum and rapidity of a given particle). 
 The particles are clustered together based on the smallest $d_{ij}$ or $d_{iB}$ until there are none left.
 The $p = -1$ is what defines the ``anti-$k_t$" method, which is resilient to the pileup and UE in the LHC~\cite{Cacciari:2008gp}.
 The jets used in this search are reconstructed with R = 0.4.
 
 \subsection{Muons}
 \label{sec:recoMuon}
%Muons are used to select events used in the background estimation (see Section~\ref{sec:bgr}).
%The Run 2 muon reconstruction and performance is described in~\cite{PERF-2015-10}.
Muons are reconstructed in the ATLAS inner detector, making use of the same standard tracking as is used for all charged particles (described in the following Section~\ref{sec:recoST}), as well as in the muon spectrometer.
Muon tracks in the MS are built primarily from hits left in the MDT chambers and the nearby triggering (RPC or TGC) chambers~\cite{PERF-2015-10} (the CSC provides some coverage in the high $\eta$ region, $|\eta| > 2.0$).
Straight-line segments are reconstructed using the hits in each MDT chamber, and a Hough transform~\cite{Illingworth:1988:SHT:51215.51221} is used to search for corresponding hits along a plane based on the particle's momentum in the magnetic field in the MS.
The hits from the RPC and TGC chambers provide the $\eta$ and $\phi$ measurements which are orthogonal to this plane. 

Candidates for muon tracks are created by combining the individual straight-line segments from different chambers.
The muon tracking algorithm starts with the segments in the middle layers of the detector, where there are the most trigger hits, and then extends to the outer and inner layers.
The segments are matched based on their relative positions and directions, and are subjected to hit multiplicity and fit quality criteria, as well as being required to point back to the approximate region of the IP. 
In general, a muon track candidate requires at least two matching segments, with the exception of candidates in the barrel-endcap transition region of the MS, where one high quality segment may be used.
During the muon track creation, one segment is allowed to be associated to multiple tracks.
These shared segments may be either assigned to the muon track with which it has the better fit, or may be allowed to be shared by the muon tracks in certain situations.
For example, if two muon tracks have segments in three different layers, they are allowed to share segments in two of those layers so long as they diverge in the third.
This allows for higher efficiency in cases when muons are very close together.

The hits in the muon track candidates are fitted using a global $\chi^2$ fit, and the track candidate is accepted given that this $\chi^2$ passes the selection criteria for the muon.
Hits may be removed from the muon track candidate if they contribute very poorly to the fit, and hits along the track trajectory may be added into the track; in both these cases, the $\chi^2$ fit is redone after the addition or removal of the hits.

Muon tracks may then be combined with the tracks formed in the ID in one of several different ways.

\textit{Combined Muons} (CB muons) may be formed when independently reconstructed tracks in the ID are matched to the muon tracks reconstructed in the MS, with a global refit that uses the tracks in both sub-detectors. 
CB muons are typically formed when a track in the MS is extrapolated inwards and matched to a track in the ID, but some CB tracks are formed by extrapolating the ID tracks outwards.

\textit{Segment-tagged muons} (ST muons) may be formed when tracks in the ID are matched with at least one MDT or CSC segment. 
ST muons are useful for muon tracks that have a low \pt~and therefore are not able cross multiple layers in the MS, or for regions with poor MS coverage.

In some cases when there is no reconstructed track in the MS, a \textit{Calorimeter-tagged muon} (CT muon) may be reconstructed. 
This is the case when an energy deposit in the calorimeters consistent with a muon is matched to a track in the ID.
While the fake rate of muons reconstructed in this manner is higher than for other types of muon reconstruction, CT muons increase the efficiency to reconstruct muon tracks in regions of the detector in which the MS loses some coverage in order to make room for cables and other services for the ID and the calorimeters, specifically in the region of $|\eta| < 0.1$.

Finally, \textit{Extrapolated muons} (ME muons) are formed when the muon track is reconstructed solely in the MS, and is required to be pointing roughly towards the IP.
In order to provide track measurements, the ME muons must pass through at least two layers of MS chambers, and at least three if they are reconstructed in the forward region.
ME muons extend the ability to reconstruct muons in the the region $2.5 < |\eta| < 2.7$, which is not covered by the ID.

If multiple muons reconstructed using different methods share the track in the ID, CB muons are chosen over ST muons and ST muons over CT.
If multiple muons reconstructed using different methods share the track in the MS, overlaps are resolved based on the of hits per track and the track quality.

Once muon tracks have been reconstructed, they are given an identification based on certain quality criteria.
In general, muons used in analyses are \textit{medium} muons (like the muons used in the background event selection, as mentioned in Section~\ref{sec:datasample}). 
Medium muons use only CB and ME muons.
Medium CB muons are required to have $\geq$ 3 hits in two or more MDT layers throughout most of the detector, and in at least one MDT layer and $\leq$ one MDT hole in the region $|\eta| < 0.1$.
ME medium muons are required to have $\geq$ three MDT or CSC layers and are required to be in the $2.5 < |\eta| < 2.7$ region, outside the ID $\eta$ acceptance.
Furthermore, in order to reduce the contamination from misidentified hadrons, the q/p significance, the absolute value of the q/p of the muons over their uncertainties, must be $<$ 7.

Muons may be identified as \textit{loose}, a category which contains all medium CB and ME muons, as well as ST and CT muons in the region $|\eta| < 0.1$.
Loose muons are used in situations that call for maximum efficiency.
\textit{Tight} muons are used in situations calling for a maximum purity at the expense of some efficiency. 
Tight muons consider only CB muons with hits in at least two different MS stations, which satisfy the medium requirements, and have a normalized $\chi^2$ which is $<$ 8.
Tight muons also have an extra selection on the q/p significance and the difference of the \pt~of the ID and MS tracks, as a function of the CB muon \pt.
Finally, \textit{high \pt} muons are those CB muons which pass the medium selection and have $\geq$ 3 hits  in three different MS stations.
The high \pt~muon identification is meant to optimize muons with \pt~$>$ 100 GeV.

\subsection{Standard tracks}
\label{sec:recoST}
Tracks are reconstructed in the inner detector using the hits deposited by charged particles. 
The main standard tracking pass used by the ATLAS experiment reconstructs tracks in an \textit{inside-out} manner.
In this \textit{inside-out} tracking pass, track seeds, collections of three space 
points\footnote{Space points may be created from one cluster in the pixel detector, which gives local 2-dimensional coordinates (on a module, giving 3-dimensional coordinates overall). In the SCT however, a cluster on a single strip will not give enough information, so space points are formed from a pair of SCT clusters on corresponding strips in the axial and stereo directions~\cite{ATLAS-CONF-2010-072}.}, 
are formed in three distinct layers in the pixel and SCT sub-detectors (seeds may be made of all pixel or all SCT space points, or a combination from both sub-detectors)~\cite{ATLAS-CONF-2010-072}.
Seeds must pass some requirements on momentum and impact parameter, as well as on the distances between the space points (tracking parameters are estimated assuming a perfectly helical track structure, ignoring any radiative energy loss, etc).
If the seeds pass these requirements, a window search is performed, and all hits in the window are put into a combinatorial Kalman filter~\cite{Fruhwirth:1987fm} in order to create track candidates.
A seed may only lead to one track candidate, and for the candidate search to be successful, the track candidate must have a minimum number of hits and those hits must not be associated to another track candidate. 

Successful track candidates are passed through an ambiguity solver. 
The purpose of this ambiguity solver is to cut down on the incidence of fake tracks, formed from combinatorial collections of hits rather than hits left by a true particle passing through the inner detector.
This is achieved by assigning a score to the track candidates. 
These scores are based on the hits in the track candidates (weighted by where in the detector the hits were), with the score being lowered by the existence of any holes\footnote{A hole is where a silicon hit was expected but none was found.} in the track.
The track scores also take into account the $\chi^2$ of the track fit.
Track candidates that survive the ambiguity solver also must pass the track parameter requirements listed for `standard' tracks in Table~\ref{table:stdvslrt}. 

Successful track candidates may be extended into the TRT (all tracks are passed through the TRT extension, tracks which fail the extension are kept in the final track collection).
The straw tube structure of the TRT means that each TRT hit can only provide a set of two-dimensional coordinates, however it can provide an average of 36 additional hits to existing tracks that are extended into the TRT.
In the TRT extension, hits compatible with the existing track candidate are assigned to that track, the quality of this extension is then evaluated using the track fit and a scoring similar to that used in the ambiguity solver.
If the TRT extension has at least 9 TRT hits and improves the quality of the track fit, then the track is considered to have been successfully extended into the TRT.
The TRT extension may fail if the track is unable to be extended into the TRT, in this case the track is still kept (this may happen if the $|\eta|$ of the track is too high, as the $|\eta|$ limits of the TRT are smaller than those of the silicon sub-detectors).
Furthermore, the track extension may be rejected, this may be due to the presence of too many 
TRT outliers\footnote{Initially, a TRT outlier is a hit for which the track associated to it passes outside of the straw tube by $\geq 100 \mu$m. After a TRT extension is rejected, all TRT hits associated to it will be marked as outliers.}
or a too high fraction of 
TRT tube hits\footnote{A TRT tube hit is one for which the signal has no leading edge, or one for which the track does not pass through the drift circle of the tube.}. 

There is a second standard tracking pass known as the \textit{outside-in} tracking pass.
In this tracking pass, standalone segments in the TRT are seeded with deposits in the ECal. 
The standalone TRT segments may be extended back into the silicon sub-detectors, using the hits that were not used during the inside-out pass (non-extended TRT standalone segments are kept and used for photon conversions).
These tracks may be slightly displaced, but are still subject to the same impact parameter requirements as the inside-out tracks listed in Table~\ref{table:stdvslrt}.

\section{Displaced tracking}
\label{sec:recoLRT}
The standard track reconstruction in ATLAS is optimized for the reconstruction of tracks left by prompt decays in the inner detector, however the efficiency falls rapidly with the displacement of the decay.
For this reason, a third tracking pass is run, known as a \textit{large radius} tracking pass (LRT), using the hits leftover from the standard tracking passes.
This large radius tracking is performed in a similar manner to the inside-out tracking, but with looser requirements on various track parameters in order to increase the efficiency of reconstructing highly displaced tracks~\cite{ATL-PHYS-PUB-2017-014}.
The differences between the requirements on track parameters and number of hits in the standard and large radius tracking passes are laid out in Table~\ref{table:stdvslrt}.
The requirements on the  longitudinal and transverse impact parameters, $|$\dO$|$~and $|$\zO$|$~are relaxed from 10~mm to 300~mm, and from 250~mm to 1500~mm respectively.
The requirements on the number of unshared silicon hits are also relaxed in order to increase the efficiency of the large radius tracking.
The seed extension furthermore uses a sequential instead of a combinatorial Kalman filter, due to the increase in the possible number of track candidates for any given seed.

\begin{table}[htb!]
	\centering
		\begin{tabular}{ c | c c }
			\hline
			Track parameter 							& Standard 		& large radius 	\\
			\hline
			Maximum  $|$\dO$|$~[mm] 					& 10 				& 300 		\\[2pt]
			Maximum  $|$\zO$|$~[mm]					& 250			& 1500 		\\[2pt]
			Minimum  \pt~[MeV] 							& 400 			& 500 		\\[2pt]
			Maximum $|\eta|$ 							& 2.7 			& 5.0 		\\[2pt]
			Minimum silicon hits 							& 7 				& 7 			\\[2pt]
			Minimum  unshared silicon hits 					& 6 				& 5 			\\[2pt]
			Maximum  silicon holes 						& 2 				& 2 			\\[2pt]
			Seed extension 							& Combinatorial	& Sequential	\\[2pt]
			\hline	
 		\end{tabular}
 	\caption{Track parameter requirements for inside-out standard and large radius tracks}
	 \label{table:stdvslrt}
\end{table}

As with the standard tracks, the large radius tracks candidates within the appropriate $|\eta|$ range are also extended into the TRT.
After the large radius tracking pass is complete, the large radius tracks are merged into a final track collection containing both the standard and large radius tracks. 


Figure~\ref{fig:effLRT_mH125mS55} demonstrates the impact of large radius tracking pass on the resulting vertex reconstruction efficiency in one of the samples used for the analysis.
As demonstrated, the vertex reconstruction efficiency without large radius tracks is greatly reduced after 10-20 mm compared to those with the large radius tracks. 

\begin{figure}[!htb]
	\centering
	\subfloat[Efficiency for all vertices]{\label{subfig:mH125mS55_all}\includegraphics[width=0.5\textwidth]{Figures/nLRT_eff_all_mH125mS55.pdf}}
	\subfloat[Efficiency for good vertices]{\label{subfig:mH125mS55_good}\includegraphics[width=0.5\textwidth]{Figures/nLRT_eff_good_mH125mS55.pdf}} 
 \caption{Comparison of the vertex reconstruction efficiency for a Higgs boson decaying to a 55 GeV mass LLP, using only standard tracking vs using standard and large radius tracking for (a) all reconstructed vertices in the signal MC samples and (b) vertices passing the IDVX selection criteria used in the analysis.}
  \label{fig:effLRT_mH125mS55}
\end{figure}

The large radius tracking pass is very CPU intensive and thus is not run by default.
The large radius tracking has to be specially reconstructed on all samples used for the analysis described here.
Information about the performance studies conducted on the large radius tracking is presented in Appendix~\ref{sec:app_lrt}.

\section{Displaced vertices in the ID}
\label{sec:recoVSI}

In addition to the specialized large radius tracking, a special secondary vertexing algorithm is used to reconstruct the decays of the LLPs in the ID.
This special vertexing algorithm takes tracks from the combined track collection which have a $|$\dO$|$~of $\geq 2$ mm~\cite{SUSY-2016-08} (a provision which is intended to exclude tracks from prompt decays).
The tracks used for the vertexing are also required to have a \pt~$>$ 1 GeV, and have either $\geq$ 2 pixel hits or a successful TRT extension, in order to increase the quality of the tracks used in the displaced vertices. 

The vertexing algorithm takes these tracks and forms a set of two-track vertex-seeds of all possible pairs of intersecting tracks using an iterative process based on the incompatibility-graph method~\cite{VtxIncGraph}.
The tracks in the vertex-seeds are required to have no hits on layers of the inner detector before the position of the vertex (between the IP and the vertex), and are required to have a hit on the next available layer of material (unless the vertex position is within or very close to the next available layer).
Any track that is shared between two or more different vertex-seeds is assigned to the one with which it has the best fit.
%, using an iterative process based on the incompatibility-graph method~\cite{VtxIncGraph}.
This process is continued until each track is only assigned to one vertex-seed. 

The vertex-seeds are merged in a multi-step process.
First, any two vertex-seeds that are within $d/\sigma_d < 3$ of each other will be merged ($d$ refers to the 3-dimensional distance between the vertex-seeds, and $\sigma_d$ is the uncertainty in the distance).
This continues until all seed vertices within $d/\sigma_d < 3$ of another seed-vertex are merged together.
During this merging step, the fits of the tracks in the newly merged vertices are re-evaluated, and poorly associated tracks are removed from the merged vertices.
All vertices are required to have a $\chi^2/\mathrm{nDoF} < 5$.
Finally, all vertices within 1 mm of each other are merged and the tracks are again re-tested for their fit with the merged vertices. 

\section{Displaced vertices in the MS}
\label{sec:recoMSVX}
The standard vertex reconstruction algorithms in the MS are designed to reconstruct decays from muons, which look very different from the displaced, hadronic decays of LLPs.
A special reconstruction algorithm was developed in Run 1 to be optimized for these decays with large numbers of low \pt~decay products~\cite{PERF-2013-01}.
This algorithm makes use of the MLs in each of the MDT chambers.
Straight-line segments are constructed with $\geq$ 3 hits in each of the MLs, and then the straight-line segments from the two MLs are matched to create tracklets in each chamber (see Figure~\ref{fig:MSVX_ML12}). 

\begin{figure}[!h]
  \centering
  	\includegraphics[width=0.45\textwidth]{Figures/MSVXRECO_fig_03.pdf}
    \caption{
The construction of tracklets from straight-line segments in each ML of the MDT chambers~\cite{PERF-2013-01}.
 }
  \label{fig:MSVX_ML12}
\end{figure} 

The tracklets within $\Delta \eta < 0.7$, $\Delta \phi < \pi/3$, are then grouped into clusters using a cone algorithm~\cite{EXOT-2017-05}, making use of the $\phi$ information in the RPC (TGC) chambers in the barrel (endcaps).
The LLP line of flight is calculated in $\eta$ and $\phi$, and the tracklet clusters are mapped into a single \textit{r--z} plane based on the $\phi$ line of flight.
The vertexing then proceeds differently in the barrel and in the endcaps due to the fact the MDT chambers in the barrel are submerged in the magnetic field, and the MDT chambers in the endcaps are not.
In the barrel, tracklets are extrapolated back towards the IP through the magnetic field, and the position of the vertex is taken to be the radius and the z-position on the line with the largest number of tracklets used to create the vertex.
The vertices are required to have a $\chi^2$ probability of $> 5\%$, if it is less than 5\%, the worst fitting tracklet is dropped and the vertex position is recalculated.
This process is repeated until the $\chi^2$ probability is sufficiently high, or until the number of tracklets is $<$ 3, at which point the vertex is discarded.
In the endcap, the tracklets are extrapolated back linearly to the endcap toroid, and the vertex position is calculated using a least-squares fit.
If tracklets are $>$ 30 cm from the vertex, they are dropped and the vertex position is recalculated.
Vertices are again required to have $\geq$ 3 tracklets. 

One repercussion of having two different vertex reconstruction algorithms in the barrel and the endcaps of the MDTs is that LLP decays in the crack region between the barrel and the endcaps may have their decay products split between algorithms.
This sometimes results in one LLP decay being reconstructed as two separate vertices in the MS, but commonly the splitting of the decay products means that there are not enough tracklets in either the barrel or in the endcap to reconstruct a single vertex.
For this reason, the $|\eta|$ region $0.8 \leq |\eta| \leq 1.3$ is excluded from the fiducial region of the muon spectrometer vertices (MSVXs)~\cite{EXOT-2017-05}.
%