\chapter{Background}
\label{sec:bgr}

As mentioned previously in Section~\ref{sec:goodIDVX}, one of the main sources of background in a search for displaced hadronic vertices in the inner detector is the interactions with the material constituting the inner detector, both the active layers, and the support material.
The vertices resulting from these material interactions should predominantly be removed with the application of the material veto, although there is still a small possibility of a reconstructed vertex escaping the material veto, or resulting from the interaction with some gas that leaked out of the detector tubes.

Other sources of background include combinatorial fakes - vertices that are made of fake tracks, or real tracks from different decays that happen to overlap, and vertices that were accidentally crossed by a track to cause the number of tracks and mass of the vertex to seem larger than it should.
Most completely fake vertices are excluded by the requirement of at least 4 tracks per vertex, and a minimum vertex mass of at least 3 GeV.

A data driven method is used to estimate the combination of all sources of background, including those which may not be easily modeled in MC samples.
Using a data-driven background estimation method also removes the complication of data/MC related systematic uncertainties.

\section{Estimation method}
The background estimation used can be presented as a modified ABCD plane, as shown in Figure~\ref{fig:ABCD1}.
The ABCD plane is used to determine the fraction of background events which contain an IDVX (presumably from background) which passes the full IDVX selection, and then to apply that fraction to events which pass the full event selection except for the requirement of an IDVX.
In this way, the number of events in the final signal region which contain an IDVX due to background can be estimated.

In the ABCD plane, region $A$ events are those which pass the full event selection, that have a PV and pass event cleaning, pass the trigger requirements (pass the muon RoI cluster trigger, do not pass the HCal jets triggers used by the CR analysis), contain a good MSVX matched to the triggering cluster, and contain an IDVX passing the full IDVX selection requirements.
Region $B$ events are those events which pass all the signal selection requirements through the existence of the MSVX, but are agnostic to the presence of any IDVX.
 

\begin{figure}[!htb]
\setlength\arrayrulewidth{1pt}
\begin{tabular}{ | b | c | c | }
\hline
\multirow{2}{*}{Has IDVX passing the}	& \multirow{2}{*}{$C$}		& \multirow{2}{*}{ {\color{red} \textbf{$A$}} }		\\[6pt]
\multirow{2}{*}{full signal selection}		&\multirow{2}{*}{}				& \multirow{2}{*}{ }							\\[6pt]
\hline
Agnostic with respect to IDVXs  		& $D$	 				& $B$ 									\\[6pt]
\hline
 \rowcolor{LightCyan}
								&  \multirow{2}{*}{Background}	& \multirow{2}{*}{Muon RoI cluster trigger}  		\\[7pt]
 \rowcolor{LightCyan}
								& \multirow{2}{*}{events}		& \multirow{2}{*}{events with a good MSVX }		\\[7pt]
\hline
\end{tabular}
\caption{The ABCD plane, with events passing the signal region requirements through the presence of a good MSVX, either containing or agnostic to signal region IDVXs ($A$ and $B$), and  background events, which contain or are agnostic to signal region IDVXs ($C$ and $D$).}
\label{fig:ABCD1}
\end{figure}

The `background events' in regions $C$ and $D$ are constructed to contain as few signal decays as possible.
The background events are required to pass the HLT trigger looking for a medium muon with a \pt~$\geq 26$ GeV and to pass vetos on the muon RoI cluster trigger and the HCal jets triggers used by the CR analysis.
The background events are also required to contain at least two muons, with \pt~$\geq 25, \, 20$ GeV, in which both muons are isolated from other activity in the event such that $\frac{\Sigma_{cone40} p_{\mathrm{T}}}{muon p_{\mathrm{T}}} < 0.30$, the total combined \pt~in a cone of $\Delta$R $< 0.4$ around the muon must be less than 30\% of the muon \pt.
The combination of these requirements minimizes the possibility of signal contamination in the background events, to the extent that $<$ 0.1\% of the signal MC samples pass the background event requirements, as demonstrated in Figure~\ref{fig:HSSbackgroundOverlap}.
In region $D$, the selection on the background events is agnostic to the presence of any IDVXs, whereas in region $C$ events, these background events are required to contain an IDVX passing the IDVX event selection laid out in Table~\ref{table:IDVXreqs}, including \mIDVX~$>$ 3 GeV, and \ntrk~$\geq$ 4.

\begin{figure}[!htb]
  \includegraphics[width=0.7\textwidth]{Figures/signal_background_overlap_log.pdf}
  \centering
  \caption{The percentage of signal MC events selected by the background event requirements.}
  \label{fig:HSSbackgroundOverlap}
\end{figure}

In the plane presented in Figure~\ref{fig:ABCD1}, a factor 
\begin{equation}
\mathrm{F} = \mathrm{N}_C/ \mathrm{N}_D
\label{eq:F_CD}
\end{equation}
can be calculated, representing the likelihood of any of the background events to contain an IDVX that passes the signal IDVX selection, based on the number of region $C$ and $D$ events, $\mathrm{N}_C$ and $\mathrm{N}_D$ respectively. 
This F, when applied to the number of events in region $B$ ($\mathrm{N}_B$) should give the number of events in region $A$ ($\mathrm{N}_A$) which pass the full signal selection but contain an IDVX from background,  

\begin{equation}
\mathrm{N}_A^{\mathrm{pred.}} = \mathrm{N}_B \times \mathrm{F} = \mathrm{N}_{\mathrm{B}} \times \mathrm{N}_{\mathrm{C}} / \mathrm{N}_{\mathrm{D}}.
\label{eq:NA_pred}
\end{equation}

There are 6,099,660 events used to populate region $D$ and 45 of these events contain an IDVX which passes the full selection, including \ntrk~$\geq 4$ and \mIDVX~$>$ 3~GeV, thus resulting in 45 region $C$ events.
Therefore, F = $\mathrm{N}_C / \mathrm{N}_D = 45/6,099,660 = (7.38 \pm 1.10(stat))\times10^{-6}$.
There are a total of 156,805 barrel and endcap combined events which pass the non-IDVX related event selections in 2016 data\footnote{The total number of events in region B is slightly different from the number of events calculated by the MSVX team in~\cite{EXOT-2017-05} due to small changes in the quality definitions for the tracks and jets used in the MSVX isolation.}.
So we expect $\mathrm{N}_A^{\mathrm{pred.}} = \mathrm{N}_B \times \mathrm{F} = \mathrm{N}_B \times \mathrm{N}_C / \mathrm{N}_D = (156,805 \pm 400) \times (7.38 \pm 1.10)\times10^{-6} $ = $ 1.16 \pm 0.18 \, (stat) $.

\section{Validation of the background estimation}
\label{sec:bgr_valid}
In order to validate the background estimation method, a set of control regions is introduced, as demonstrated in Figure~\ref{fig:ABCD_CR1}.
Here, region $C'$ events are background events, but instead of being agnostic to the presence of any IDVXs, such as those in region $D$, and instead of being required to contain an IDVX passing the full signal selection, such as those in region $C$, they are required to contain an IDVX passing the full signal selection, except for the requirement on the number of tracks, which has been changed from $\geq$ 4 tracks, to exactly 2 tracks.
Similarly, the region $B'$ events are required to pass the full requirements for the signal region, except the IDVX has to have \ntrk~= 2 instead of \ntrk~$\geq$ 4.
IDVXs with \ntrk~= 2 are chosen because the 2-track IDVXs should be dominated by background, even in events otherwise passing the signal selection.

\begin{figure}[!htb]
\setlength\arrayrulewidth{1pt}
  \centering
\begin{tabular}{ | b | c | c | }
\hline
Has IDVX, \ntrk~$\geq$ 4, \mIDVX~$>$ 3 GeV	& $C$	 					& {\color{red} \textbf{$A$}}				\\[5pt]
\hline
Has IDVX, \ntrk~= 2, \mIDVX~$>$ 3 GeV		& $C'$ 						& $A'$								\\[5pt]
\hline
Agnostic with respect to IDVXs  			& $D$	 					& $B$ 								\\[5pt]
\hline
 \rowcolor{LightCyan}
									&  \multirow{2}{*}{Background}		& \multirow{2}{*}{Muon RoI cluster trigger}		\\[6pt]
 \rowcolor{LightCyan}
									& \multirow{2}{*}{events}			& \multirow{2}{*}{events with a good MSVX }	\\[6pt]
\hline
\end{tabular}
\caption{The ABCD plane, with the addition  of the $B'$ and $C'$ regions, containing IDVXs with a \mIDVX~$>$ 3 GeV and \ntrk~= 2.}
\label{fig:ABCD_CR1}
\end{figure}

Like the factor F that was calculated using the plane in Figure~\ref{fig:ABCD1}, a factor F' can be calculated using the new control regions in the plane in Figure~\ref{fig:ABCD_CR1}.
Here, 

\begin{equation}
\mathrm{F'} = \mathrm{N}_{C'} / \mathrm{N}_D, 
\label{eq:Fp_CpD}
\end{equation}

and the expected number of events in region $A'$ is

\begin{equation}
\mathrm{N}_{A'}^{\mathrm{pred.}} = \mathrm{N}_B \times \mathrm{F'} = \mathrm{N}_B \times \mathrm{N}_{C'} / \mathrm{N}_D.
\label{eq:NAp_pred}
\end{equation}

%Given the 6,099,660 region $D$, background type events used in the validation of the background estimation method, and 156,805 region $B$ events
It is found that 438,351 of the region $D$ events contain at least one 2-track IDVX as described above, thus $\mathrm{F'} = \mathrm{N}_{C'} / \mathrm{N}_D = 438,351/6,099,660  = (7.19 \pm 0.011\, (stat))\times10^{-2}$.
Therefore, the predicted number of region $A'$ events becomes $\mathrm{N}_{A'}^{\mathrm{pred.}} = \mathrm{N}_B \times \mathrm{F'} = \mathrm{N}_B \times \mathrm{N}_{C'} / \mathrm{N}_D =  (156,805 \pm 400) \times (7.19 \pm 0.011)\times10^{-2} $ = $ 11,268.8\pm 46.1 \, (stat) $.
The actual number of events found in region $A'$ was 11,470, which is within 2\% of the predicted number.

There are sufficient statistics to examine the distributions of the 2-track vertices that are found in the $A'$ and $C'$ validation regions to ensure that there isn't any significant bias introduced by the selection of the background events.
Figure~\ref{fig:BackgroundVtxSyst_R_z} shows the normalized R and z distributions for the 2-track vertices found in the validation regions $A'$ and $C'$, as well as the ratios of the of the normalized $A'$ distributions to the $C'$ distributions.
Along both the vertex R and z, the ratio of the normalized distributions is fairly consistent with one, after taking into account the statistical uncertainties.
Any additional differences in the R and z distributions would be covered by a systematic uncertainty of $\geq \pm10$\% (as represented by the red dashed lines in the ratio plots).

\begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:vtx2R}\includegraphics[width=0.5\textwidth]{Figures/bgrSyst_R_2trk.pdf}}
	\subfloat[]{\label{subfig:vtx2zi}\includegraphics[width=0.5\textwidth]{Figures/bgrSyst_z_2trk.pdf}} 
	\\
	\subfloat[]{\label{subfig:vtx2RRatio}\includegraphics[width=0.5\textwidth]{Figures/bgrSyst_RRatio_2trk.pdf}}
	\subfloat[]{\label{subfig:vtx2zRatio}\includegraphics[width=0.5\textwidth]{Figures/bgrSyst_zRatio_2trk.pdf}}
 	\caption{The normalized distributions of the (a) R and (b) z positions of the 2-track vertices for regions $A'$ and $C'$, and (c),(d) the ratios of the $A'$ region to the $C'$ region.
	The red dashed lines in (c) and (d) represent $1\pm 0.1$.}
  \label{fig:BackgroundVtxSyst_R_z}
\end{figure}

Figure~\ref{fig:BackgroundVtxSyst_eta_phi} shows the normalized $\eta$ and $\phi$ distributions for the 2-track vertices found in the validation regions $A'$ and $C'$, as well as the ratios of the of the normalized $A'$ distributions to the $C'$ distributions.
The ratios of the normalized distributions vs $\eta$ and $\phi$ are also mostly consistent with one within the statistical uncertainties.
Once again, any differences in the vertex distributions introduced by the different event selections are easily covered by a small systematic.

\begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:vtx2eta}\includegraphics[width=0.5\textwidth]{Figures/bgrSyst_eta_2trk.pdf}}
	\subfloat[]{\label{subfig:vtx2phi}\includegraphics[width=0.5\textwidth]{Figures/bgrSyst_phi_2trk.pdf}} 
	\\
	\subfloat[]{\label{subfig:vtx2etaRatio}\includegraphics[width=0.5\textwidth]{Figures/bgrSyst_etaRatio_2trk.pdf}}
	\subfloat[]{\label{subfig:vtx2phiRatio}\includegraphics[width=0.5\textwidth]{Figures/bgrSyst_phiRatio_2trk.pdf}}
 	\caption{The normalized distributions of the (a) $\eta$ and(b) $\phi$ of the 2-track vertices for regions $A'$ and $C'$, and (c),(d) the ratio of the $A'$ region to the $C'$ region.
	The red dashed lines in (c) and (d) represent $1\pm 0.1$.}
  \label{fig:BackgroundVtxSyst_eta_phi}
\end{figure}

Figure~\ref{fig:BackgroundVtxSyst_mass} shows the normalized 2-track vertex distributions versus the vertex mass as well as the ratio of the normalized distributions.
Although the statistics become very limited for \mIDVX~$>$ 10~GeV, the ratio is approximately consistent with one within statistical uncertainties.
Figures~\ref{fig:BackgroundVtxSyst_R_z},~\ref{fig:BackgroundVtxSyst_eta_phi}, and~\ref{fig:BackgroundVtxSyst_mass} demonstrate that there is no significant bias in the distributions of the vertices in the validation regions introduced by the background event selection compared to the signal selection.
This increases the confidence in the use of the background events to develop the background estimation.

\begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:vtx2VVM}\includegraphics[width=0.5\textwidth]{Figures/bgrSyst_mass_2trk.pdf}} 
	\subfloat[]{\label{subfig:vtx2VVMRatio}\includegraphics[width=0.5\textwidth]{Figures/bgrSyst_massRatio_2trk.pdf}}
  \caption{The normalized distributions of the \mIDVX~of 2-track vertices for regions $A'$ and $C'$ (a), and the ratio of the $A'$ region to the $C'$ region (b).
  The red dashed lines in (b) represent $1\pm 0.1$.}
  \label{fig:BackgroundVtxSyst_mass}
\end{figure}

Due to the fact that different sources of background are likely to be the main contributors to IDVXs with \ntrk~= 2, compared to IDVXs with a higher number of tracks, a second set of control regions was developed, which are demonstrated in Figure~\ref{fig:ABCD_CR2}.

\begin{figure}[!htb]
\setlength\arrayrulewidth{1pt}
  \centering
\begin{tabular}{ | b |  c | c | c | }
\hline
\multirow{2}{*}{Has IDVX, nTrks $\geq$ 4,}	& \multirow{2}{*}{$C$}		&								&  \multirow{2}{*}{\color{red} \textbf{$A$}}	\\[5pt]
\multirow{2}{*}{mass $>$ 3 GeV} 			& \multirow{2}{*}{}			&								& \multirow{2}{*}{} 					\\[5pt]
\hline
\multirow{2}{*}{Has IDVX, nTrks = 3,}			& \multirow{2}{*}{$C''$} 		& \multirow{2}{*}{$A''$} 				&								\\[5pt]
\multirow{2}{*}{1 $<$  mass $<$ 3 GeV}		& \multirow{2}{*}{} 			& \multirow{2}{*}{} 					&								\\[5pt]
\hline
Agnostic with respect to IDVXs  			& $D$					& $B''$							& $B$							\\[5pt]
\hline
 \rowcolor{LightCyan}
									& \multirow{3}{*}{Background}	& \multirow{3}{*}{Muon RoI cluster}  		& \multirow{3}{*}{Muon RoI cluster}	 	\\[12pt]
 \rowcolor{LightCyan}	
									& \multirow{3}{*}{events}		& \multirow{3}{*}{trigger events}			& \multirow{3}{*}{trigger events w/}		\\[13pt]
\rowcolor{LightCyan}
									& \multirow{3}{*}{}			& \multirow{3}{*}{agnostic to MSVXs}		& \multirow{3}{*}{a good MSVX}		\\[12pt]
\hline
\end{tabular}
\caption{The ABCD plane, with the addition of the $A''$, $B''$ and $C''$ regions, containing IDVXs with 1 GeV $<$ \mIDVX~$<$ 3 GeV~and \ntrk~= 3, and events that pass the muon RoI cluster trigger but that don't necessarily contain any good MSVXs.}
\label{fig:ABCD_CR2}
\end{figure}

Here, the region $C''$ events are background events containing an IDVX passing all the IDVX requirements described in Table~\ref{table:IDVXreqs} except those on the vertex mass and on the number of tracks, instead these vertices are required to have a vertex mass just lower than those in the signal region, 1 $<$ \mIDVX~$<$ 3 GeV, and exactly 3 tracks.
A new type of events is added; region $B''$, events that pass all the trigger requirements used for the events in the signal region, but which are agnostic to the presence of any MSVXs.

\begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:SvBmuVtx_lowMass_mH125}\includegraphics[width=0.5\textwidth]{Figures/svb_mH125_0_1_muvtx_nTrks.pdf}} 
	\subfloat[]{\label{subfig:SvBmuVtx_lowMass_mH1000}\includegraphics[width=0.5\textwidth]{Figures/svb_mH1000_0_1_muvtx_nTrks.pdf}}
	\\
	\subfloat[]{\label{subfig:SvBmuVtx_lowMass_BS_mH125}\includegraphics[width=0.5\textwidth]{Figures/svb_mH125_0_1_muvtx_nTrks_BS.pdf}} 
	\subfloat[]{\label{subfig:SvBmuVtx_lowMass_BS_mH1000}\includegraphics[width=0.5\textwidth]{Figures/svb_mH1000_0_1_muvtx_nTrks_BS.pdf}}
  \caption{Scaled distributions of signal (from signal MC samples) and background (from region $D$) vertices expected to be found in region $B"$.
   Shown for (a) 125~GeV Higgs$\rightarrow ss$ and (b) 1000~GeV $\Phi \rightarrow ss$ (b). The IDVXs are required to have vertex mass 1 $<$ mass $<$ 3~GeV and exactly 3 tracks. The ratios of the scaled number of background vertices/scaled number of signal vertices are shown in (c) and (d).}
  \label{fig:SvBmuVtx_lowMass}
\end{figure}

As shown in Figure~\ref{fig:SvBmuVtx_lowMass} in these events the amount of signal contamination for the 3-track, lower mass IDVXs described above is determined to be at most 3\%, based on current limits, which is significantly lower than it would have been in region $B$ type events, and so it is safe to proceed with validation of the background estimation.
Region $A''$ is thus defined as events passing the muon RoI cluster trigger (and not the HCal jets triggers), which contain a low-mass, 3-track vertex.

Once again, a factor $\mathrm{F}''$ can be defined, $\mathrm{F''} = \mathrm{N}_{C''} / \mathrm{N}_D$.
In this case, out of the 6,099,660 events used to populate region $D$, 765 events were found to contain a low mass, 3-track vertex, thus  $\mathrm{F''} = 765/6,099,660 = (1.25 \pm 0.045 \, (stat))\times10^{-4}$.
There were 13,953,316 events used to populate region $B''$, so the predicted number of events in $A''$ becomes $\mathrm{N}_{A''}^{\mathrm{pred.}} = \mathrm{N}_{B''} \times \mathrm{F''} = \mathrm{N}_{B''} \times \mathrm{N}_{C''} / \mathrm{N}_D = (1.3953316 \times 10^7 \pm 3.7 \times 10^3) \times (1.25 \pm 0.045)\times10^{-4} $ = $ 1,750 \pm 64 \, (stat) $.
The total number of events found to be in region $A''$ was 2,132, which is within 25\% of the predicted number of events.
To be conservative, because the second set of validation regions are similar to, but not exactly like the ABCD regions, the systematic uncertainty on the background estimation method is taken to be 50\%.

\subsection{Jet multiplicity impact on the background estimation}
The jet multiplicity distributions are not exactly the same in the different regions used in the modified ABCD plane, so to provide more confidence that this difference will not impact the accuracy of the background estimation, the impact on the background estimation of the jet multiplicities is examined.
The jets considered are those that pass standard jet cleaning selections and have a \pt~$\geq 20$~GeV.
The jet multiplicities are examined separately considering jets that are selected to have passed the JVT selection and with the selection agnostic to the JVT requirement. 
The inclusion of the JVT requirement reduces the contribution from the pileup jets in the events.

Jet multiplicities are compared between the three ABCD regions (and validation regions) that are agnostic to the presence of vertices: $D$, $B$, and $B''$.

Figure~\ref{fig:JetMult_Total} shows the overall jet multiplicity distributions in regions $D$, $B$, and $B''$, considering all jets (passing the jet cleaning and \pt~requirements) in Figure~\ref{subfig:jets_all} and jets that additionally pass the JVT requirement in Figure~\ref{subfig:jets_jvt_all}.
The normalized distributions are shown in Figures~\ref{subfig:jets_all_norm} and~\ref{subfig:jets_all_jvt_norm} respectively.

\begin{figure}[!htb]
	\subfloat[Jet multiplicity without JVT consideration]{\label{subfig:jets_all}\includegraphics[width=0.5\textwidth]{Figures/jetMult_all.pdf}}
	\subfloat[Jet multiplicity with JVT consideration]{\label{subfig:jets_jvt_all}\includegraphics[width=0.5\textwidth]{Figures/jetMult_jvt_all.pdf}}
  \centering
  \caption{The jet multiplicity in events used in the background estimation, for region $D$, $B$, and $B''$ events. 
  Jet multiplicities (a) agnostic to and (b) passing the JVT requirement.}
  \label{fig:JetMult_Total}
\end{figure}

From Figures~\ref{fig:JetMult_Total} and~\ref{fig:jets_dist_norm_ratio} it can be seen that a greater proportion of the events populating region $D$ have no jets (whether taking into account or not the JVT requirement), than of the events in regions $B$ or $B''$.
Region $D$ also has a greater fraction of events with 9 or more jets than region $B$.
This is particularly clear in the ratios of the normalized distributions, Figures~\ref{subfig:jets_all_ratio} and~\ref{subfig:jets_all_jvt_ratio}, which show the increased fraction of region $D$ events compared to region $B$ events with 0-1 or $\geq$ 9 jets. 
The validation region, region $B'$, also has somewhat different jet multiplicity distributions than those found in the region $B$ events, tending to have more jets per event.
This difference is expected, as there are isolation requirements placed on the MSVX found in the region $B$ events, which are not required in the region $B''$ events, these isolation requirements which reject MSVXs which are produced nearby to jet activity in the event, will disfavor events with higher jet multiplicities.

\begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:jets_all_norm}\includegraphics[width=0.5\textwidth]{Figures/jetMult_norm.pdf}}
	\subfloat[]{\label{subfig:jets_all_jvt_norm}\includegraphics[width=0.5\textwidth]{Figures/jetMult_jvt_norm.pdf}}
\\
	\subfloat[]{\label{subfig:jets_all_ratio}\includegraphics[width=0.5\textwidth]{Figures/jetMult_normRatio.pdf}}
	\subfloat[]{\label{subfig:jets_all_jvt_ratio}\includegraphics[width=0.5\textwidth]{Figures/jetMult_jvt_normRatio.pdf}}

	\caption{The normalized jet multiplicity distributions agnostic to (a) and passing (b) the JVT requirement on the jets in regions $D$, $B$, and $B''$.
	The ratios of the normalized distributions are shown in (c) and (d).}
	\label{fig:jets_dist_norm_ratio} 
\end{figure}

To examine how these differences in the jet multiplicities in the different ABCD regions impact the background estimation, the factors $\mathrm{F''}$ and $\mathrm{F'}$ are compared versus the jet multiplicity.
As defined in Equations~\ref{eq:Fp_CpD} and~\ref{eq:NAp_pred}, $\mathrm{F'} = \mathrm{N}_{C'} / \mathrm{N}_D$, and since it is predicted that $\mathrm{N}_{A'}= \mathrm{N}_B \times \mathrm{F'} $, it is possible to compare $\mathrm{F'} = \mathrm{N}_{C'} / \mathrm{N}_D$ and $\mathrm{F'} = \mathrm{N}_{A'} / \mathrm{N}_B$.
Similarly it is possible to compare the $\mathrm{F''}$ factors calculated from $\mathrm{F''} = \mathrm{N}_{C''} / \mathrm{N}_D$ or from $\mathrm{F''} = \mathrm{N}_{A''} / \mathrm{N}_{B''}$.

Figure~\ref{fig:JetMult_2TrkRates} demonstrates the $\mathrm{F'}$ factors based on regions $C'$ and $D$ compared to $A'$ and $B$ as a function of the jet multiplicity in the region $B$ and $D$ events.
There is a positive correlation between the jet multiplicity in the events and the magnitude of the $\mathrm{F'}$ factors, meaning the increased jet multiplicity increases the likelihood that a given event will contain at least one 2-track vertex.
The increase of the $\mathrm{F'}$ factors are similar within statistical uncertainties vs the jet multiplicity for $\mathrm{F'}$ based on regions $C'$ and $D$ or $A'$ and $B$.

\begin{figure}[!htb]
	\subfloat[]{\label{subfig:jetmult_2trknom}\includegraphics[width=0.5\textwidth]{Figures/jetMult_2trknom.pdf}}
	\subfloat[]{\label{subfig:jetmult_2trknom_jvt}\includegraphics[width=0.5\textwidth]{Figures/jetMult_2trknom_jvt.pdf}}
  \centering
  \caption{ The factor $\mathrm{F'} = \mathrm{N}_{C'} / \mathrm{N}_D, \, \mathrm{F'} = \mathrm{N}_{A'} / \mathrm{N}_{B}$, vs the jet multiplicity, considering jets (a) agnostic to or (b) passing the JVT selection.}
  \label{fig:JetMult_2TrkRates}
\end{figure}

The dependence of the $\mathrm{F''}$ factors on the jet multiplicity in the events is shown in Figure~\ref{fig:JetMult_3TrkRates}.
The $\mathrm{F''}$ factors show a stronger dependence on the jet multiplicity than the $\mathrm{F'}$ because the 2-track vertices are more likely to be formed from the random crossing of two unrelated (fake or real) tracks, while vertices with three tracks are more likely to be associated with activity in the calorimeters.
Once again the functions of $\mathrm{F''}$ factors calculated based on the $C''$ and $D$ regions and the $A''$ and $B''$ regions vs jet multiplicity are somewhat similar within statistical uncertainties.
Both the consistency within the two $\mathrm{F''}$ factors and the approximately linear relationship between the $\mathrm{F''}$ factor and the jet multiplicity in the $B''$ and $D$ events are stronger for jet multiplicity taking into account only jets passing the JVT requirement; the number of non-pileup jets in the events.

\begin{figure}[!htb]
	\subfloat[]{\label{subfig:jetmult_3trklow}\includegraphics[width=0.5\textwidth]{Figures/jetMult_3trklow.pdf}}
	\subfloat[]{\label{subfig:jetmult_3trklow_jvt}\includegraphics[width=0.5\textwidth]{Figures/jetMult_3trklow_jvt.pdf}}
  \centering
  \caption{ The factor $\mathrm{F''} = \mathrm{N}_{C''} / \mathrm{N}_D, \, \mathrm{F''} = \mathrm{N}_{A''} / \mathrm{N}_{B''}$, vs the jet multiplicity, considering jets (a) agnostic to or (b) passing the JVT selection.}
  \label{fig:JetMult_3TrkRates}
\end{figure}

As discussed in Section~\ref{sec:bgr_valid}, the actual number of events in $A'$ is within 2\% of the predicted number of events based on the $\mathrm{F'}$ factor, while the predicted number of events in region $A''$ is within 25\% of the actual number found.
To determine the impact of the scaling the jet multiplicity in all regions to that found in the region $B$ events, the total $\mathrm{F''}$ factors can be calculated from the events in each region in each jet multiplicity bin, and then recalculated based on the scaling.

The overall  $\mathrm{F''}$ factors based on regions $C''$ and $D$, and $A''$ and $B''$ can be calculated as 
\begin{equation}
\mathrm{F''} = \frac{ \sum^{bin}\mathrm{N}_{C''}^{\mathrm{bin}} }{ \sum^{bin}\mathrm{N}_{D}^{\mathrm{bin}} } 
\end{equation}
and
\begin{equation}
 \mathrm{F''} = \frac{ \sum^{bin}\mathrm{N}_{A''}^{\mathrm{bin}} }{ \sum^{bin}\mathrm{N}_{B''}^{\mathrm{bin}} }
\end{equation}
respectively, where the bins are the jet multiplicity bins.
Additional per-bin factors can be calculated based on the ratios of the normalized jet multiplicity distributions shown in Figures~\ref{subfig:jets_all_ratio} and~\ref{subfig:jets_all_jvt_ratio}, such that 
$f_{B''}^{\mathrm{'' bin}} = \mathrm{N}_{B}^{\mathrm{bin}}/\mathrm{N}_{B''}^{\mathrm{bin}} $ 
or $f_D^{\mathrm{'' bin}} = \mathrm{N}_{B}^{\mathrm{bin}}/\mathrm{N}_{D}^{\mathrm{bin}}$.
The $f''^{\mathrm{bin}}$ factors allow the jet multiplicity in the events to be rescaled to match the jet multiplicity in the region $B$ events since the factor F is applied to the region $B$ events in the final background estimation.

The rescaled $\mathrm{F''}$ factors then are calculated via 
\begin{equation}
\mathrm{F''} = \frac{ \sum^{bin} f_{B''}^{\mathrm{'' bin}} \times \mathrm{N}_{A''}^{\mathrm{bin}} }{ \sum^{bin} f_{B''}^{\mathrm{'' bin}} \times \mathrm{N}_{B''}^{\mathrm{bin}} }
\end{equation}
and 
\begin{equation}
\mathrm{F''} = \frac{ \sum^{bin} f_{D}^{\mathrm{'' bin}} \times \mathrm{N}_{C''}^{\mathrm{bin}} }{ \sum^{bin} f_{D}^{\mathrm{'' bin}} \times \mathrm{N}_{D}^{\mathrm{bin}} }. 
\end{equation}
The rescaled $\mathrm{F''}$ factors can be calculated using the jet multiplicities agnostic to, or considering, the JVT jet requirements.

As shown in Figure~\ref{fig:JetMult_Rates_Scaled}, the total $\mathrm{F''}$ factor calculated from the $C''$ and $D$ regions differs significantly from the total $\mathrm{F''}$ factor calculated from the $A''$ and $B''$ regions even after taking into account the statistical uncertainties (as shown by the black points).
The recalculated $\mathrm{F''}$ factors taking into account the jet multiplicities that are agnostic to the JVT requirement, represented by the blue filled triangles in Figure~\ref{fig:JetMult_Rates_Scaled}, differ only by about 4\%, as shown by the green dashed line, which is within the statistical uncertainty.
The recalculated $\mathrm{F''}$ factors taking into account the jet multiplicities that take into account the JVT requirement, represented by the blue open triangles, agree within 5\%, which is also within the statistical uncertainties, shown by the purple dashed line.
Thus, re-scaling by the jet multiplicity differences between the regions (scaling to match the jet multiplicity in region $B$), provides closure, reducing the differences between the $\mathrm{F''}$ factors calculated from the $C''$ and $D$ regions or $A''$ and $B''$ regions to within the statistical uncertainties.
This is true whether the jet multiplicity used for the scaling takes into account the JVT requirement or not.

\begin{figure}[!htb]
 \includegraphics[width=0.7\textwidth]{Figures/jetMult_totVtx_3trklow.pdf}
  \centering
  \caption{ The overall factor $\mathrm{F''}$, for the 3 track vertices, low mass vertices, unscaled (black points), scaled based on the region $B$ events jet multiplicity (blue filled triangles), and scaled based on the region $B$ jet multiplicity taking into account JVT requirements (blue open triangles). Dashed lines indicate the level of agreement between the calculations of $\mathrm{F''}$, before and after scaling to the region $B$ events jet multiplicity.}
  \label{fig:JetMult_Rates_Scaled}
\end{figure}

If the same scaling method is applied to the factor F that is used to predict the number of background events found in region A, the rescaled 
\begin{equation}
\mathrm{F}_{rescale} = \frac{ \sum^{bin}f^{\mathrm{bin}} \times \mathrm{N}_{C}^{\mathrm{bin}} }{ \sum^{bin}f^{\mathrm{bin}} \times \mathrm{N}_{D}^{\mathrm{bin}} },
\end{equation} 
using  $f^{\mathrm{bin}} = \mathrm{N}_{B}^{\mathrm{bin}}/\mathrm{N}_{D}^{\mathrm{bin}}$.
The rescaled F factor calculated when considering the jet multiplicity agnostic to the JVT selection, $\mathrm{F}_{rescale} = 8.6 \times 10^{-6}$, differs from the unscaled F =  $(7.38 \pm 1.10(stat))\times10^{-6}$ by 15\%, and is approximately within the statistical uncertainty on the unscaled F.
Considering the rescaled F factor calculated using the multiplicity of jets passing the JVT selection, $\mathrm{F}_{rescale} = 8.1 \times 10^{-6}$, differs from the unscaled F by 9.2\%, less than the statistical uncertainty.
Thus, while the closure provided in the 3-track validation regions after the jet multiplicity scaling 
%, and the subsequent agreement of the $\mathrm{F''}$ factors calculated from the $C''$ and $D$ versus the $A''$ and $B''$ regions 
provides more confidence in the effectiveness of the background estimation method and validation, the impact on the final background estimate is ultimately very small (within statistical uncertainty).
Since the jet multiplicity scaling would have such a small impact on the background estimation method, it was determined that it was more straightforward to leave the estimate unscaled, and take the systematic uncertainty to be based on the largest difference between the predicted and observed numbers of events found in the unscaled validation regions.

\section{Expected number of background events}

As discussed previously, the predicted number of events from background passing the full signal region requirements is $\mathrm{N}_A^{\mathrm{pred.}} = \mathrm{N}_B \times \mathrm{F} = \mathrm{N}_{\mathrm{B}} \times \mathrm{N}_{\mathrm{C}} / \mathrm{N}_{\mathrm{D}}$.
The total number of events in region $B$ is 156,805, thus the predicted number of region $A$ events becomes $\mathrm{N}_A = \mathrm{N}_B \times \mathrm{F} = \mathrm{N}_B \times \mathrm{N}_C / \mathrm{N}_D = (156,805 \pm 400) \times (7 \pm 1.1)\times10^{-6} $ = $ 1.2 \pm 0.2 \,(stat) \pm 0.6 \, (syst.)$.
