\chapter{Systematic Uncertainties}
\label{sec:syst}

Many sources of statistical uncertainties are introduced during an experimental search for new particles.
Systematic uncertainties may come from a variety of sources such as the parton distribution functions (PDFs) used to create the MC samples, and the differences introduced by using MC samples for studies and ultimately performing the search in data.
Uncertainties from the differences in the reconstruction efficiency in data and in MC samples, from the trigger scale factors, the pileup re-weighting, and the PDF fits are summarized in the following section.
None of the studies described here were performed by the author, but are necessary to the final result, credit is given to the analyses and analyzers who performed the studies. 

\section{Systematic uncertainties on displaced tracking and vertex reconstruction in the ID}
\label{sec:syst_VSI_LRT}
%\textit{Need to give credit to Jackson, both for the study in general, and for Figures~\ref{fig:KSKinematics} and~\ref{fig:LRTKSYield}.}

In order to determine any systematic uncertainties introduced by the differences in the reconstruction of the displaced IDVXs, reconstructed vertices from $K_S^0 \rightarrow \pi^+\pi^-$ decays were compared in data and in MC samples\footnote{This study comparing the $K_S^0 \rightarrow \pi^+\pi^-$ decays in data and in MC samples was performed by University of Massachusetts Amherst student, Jackson Burzynski.}. 
The $K_S^0$ vertices are expected to be well modeled in MC simulation and may be reconstructed with standard tracks and/or tracks reconstructed during the large radius tracking pass.
Because the systematic uncertainties due to the standard tracking algorithm are understood well by the ATLAS collaboration~\cite{ATL-PHYS-PUB-2015-051}, comparing the $K_S^0$ vertices with two standard tracks and the $K_S^0$ vertices with two large radius tracks provides an estimate of the systematic uncertainties introduced by the large radius tracking and displaced vertexing algorithms.

The data events used for this study were those events in the RPVLL filter stream (all the events selected to be reconstructed with the large radius tracking and displaced IDVX algorithms), that pass the GRL and the quality standards for the SCT, LAr, and Tile sub-detectors.
The di-jet MC events used are those in the \pt~slices outlined in Table~\ref{table:JZXW}.
These \pt~slices are chosen so that the shape of the jet \pt~distribution in the di-jet slices most closely matched the shape of the jet \pt~distribution in the signal region in data (see Figure~\ref{fig:jetPTcomparedataMC}).
No further requirements are placed on either the data or MC sample events used, in order to minimize the impact of the statistical uncertainty.  

\begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:jetpTs_low}\includegraphics[width=0.5\textwidth]{Figures/JetpT_lowpT_JZXW_ZMUMU_RPVLL.pdf}}
	\subfloat[]{\label{subfig:jetpTs_rebin}\includegraphics[width=0.5\textwidth]{Figures/JetpT_rebinned_JZXW_ZMUMU_RPVLL.pdf}} 
  \caption{ The comparison of the jet \pt~distributions in the di-jet MC samples and in the signal region in data (minus the requirement of an IDVX). 
  The jet \pt~region from 0-200 GeV is shown in (a) and an expanded region from 0-1000 GeV is shown in (b).
  The jet \pt~distribution in data is compared to the jet \pt~distributions using the di-jet slices JZ1W-JZ7W or JZ2W-JZ7W to show that the inclusion of the JZ1W slice is important to for the shape of the low \pt~distribution to match that found in data.
  }
  \label{fig:jetPTcomparedataMC}
\end{figure}

Preliminary $K_S^0$ vertex candidates are selected as those displaced vertices that pass the fiducial volume selections described in Section~\ref{sec:goodIDVX}, $K_S^0$ $R,|z| < 300$~mm, radial distance from the PV $>$ 4~mm, and passing the material and disabled module vetos.
The preliminary $K_S^0$ vertex selection also requires $\chi^2 / \mathrm{nDoF}$ $<$ 5.
Furthermore, the preliminary $K_S^0$ vertex candidates are required to have exactly 2 tracks, a decay length $\geq 15$~mm, and an invariant mass of 450~MeV $< m_{\mathrm{vertex}} < $ 550~MeV.
The last two selections reduce the potential signal contamination to a trivial level without significantly impacting the efficiency to select $K_S^0$ vertices.

Any potential background is calculated by subtracting the number of vertices in the invariant mass sidebands of 350~MeV $< m_{\mathrm{vertex}} < $ 450~MeV and 550~MeV $< m_{\mathrm{vertex}} < $ 650~MeV, from the number of vertices in the 450~MeV $< m_{\mathrm{vertex}} < $ 550~MeV invariant mass region.
The total number of final $K_S^0$ vertex candidates is thus computed via Equation~\ref{eq:KS_background},

\begin{equation}
N_{K_S^0} = N_{450 < m < 550}  - \frac{ N_{350 < m < 450} + N_{550 < m < 650} }{2}.
\label{eq:KS_background}
\end{equation}


The distributions of the $K_S^0$ candidate decay radius $R$~[mm], longitudinal decay position $z$~[mm], decay length [mm], and invariant mass [GeV] are shown in Figure~\ref{fig:KSKinematics}.
The vertices shown in Figure~\ref{fig:KSKinematics} have no requirement on the tracking algorithm used to reconstruct the constituent tracks.
There is good agreement demonstrated between the $K_S^0$ candidate distributions in data and in MC samples, confirming that the $K_S^0$ vertices are well modeled in MC simulation.

\begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:KS_R}\includegraphics[width=0.5\textwidth]{Figures/KS_R.pdf}}
	\subfloat[]{\label{subfig:KS_z}\includegraphics[width=0.5\textwidth]{Figures/KS_z.pdf}} 
	\\
	\subfloat[]{\label{subfig:KS_decayL}\includegraphics[width=0.5\textwidth]{Figures/KS_L.pdf}}
	\subfloat[]{\label{subfig:KS_M}\includegraphics[width=0.5\textwidth]{Figures/KS_M.pdf}}
  \caption{ The data vs MC comparison of the radial decay position of the $K_S^0$ vertices, R [mm] is shown in (a), the longitudinal decay position of the vertices, z [mm] is shown in (b), the decay length of the vertices [mm] is shown in (c), and the invariant mass of the vertices [MeV] is shown in (d).
	  The dashed lines shown in (a) represent the radial position of the layers of material in the pixel detector and the first layer of the SCT.
	All reconstructed $K_S^0$ candidates are included, whether they were reconstructed with standard or large radius tracks.
  	\textit{Figures were created by J. Burzynski.}}
  \label{fig:KSKinematics}
\end{figure}

The yield of the $K_S^0$ vertices reconstructed with large radius tracks only is shown in Figure~\ref{fig:LRTKSYield}, as a function of the  $K_S^0$ decay radius $R$~[mm].
The distribution in data is normalized so that the total vertex collections in data events and in MC sample events had the same number of $K_S^0$ vertex candidates reconstructed using only standard tracks.
This is done to account for any differences in the total number of $K_S^0$ candidates existing in the data and MC samples.
The normalization is applied to the distribution in data rather than to the distribution in the di-jet MC samples due to a limitation in the MC statistics.


\begin{figure}[!htb]
  \includegraphics[width=0.7\textwidth]{Figures/LRTKSYield_vs_R.pdf}
  \centering
  \caption{The yield of the $K_S^0$ vertices reconstructed with only large radius tracks, as a function of the radial decay position of the vertex, $R$ [mm].
  		The vertex collection in data is normalized such that the data and MC sample events each had the same number of vertices reconstructed using only standard tracks.
		\textit{Figure was created by J. Burzynski.}
		}
  \label{fig:LRTKSYield}
\end{figure}

Figure~\ref{fig:LRTKSYield} demonstrates the ratio of large radius-track-only $K_S^0$ candidates in data to those in MC samples in each of five decay radius $R$~[mm] bins.
The largest difference from one in any bin is approximately 20\%.
To be conservative, a systematic uncertainty of 20\% is applied to the IDVX reconstruction efficiency.

\section{Uncertainty on integrated luminosity}
The uncertainty on the integrated luminosity for the 2016 dataset is 2.2\%.
This uncertainty is derived using a methodology like that which is described in Ref.~\cite{DAPR-2013-01}, using the LUCID-2 detector~\cite{Avoni_2018} to perform baseline luminosity measurements from a calibration of the luminosity scaling using x-y beam-separation scans.

\section{MSVX reconstruction efficiency}
The MS analysis used punch-through jets in data and di-jet MC events to assess the systematic uncertainty associated to the MSVX reconstruction.
The average number of muon segments found in the punch-through jet cones were compared between data and MC events as a function of the leading jet \pt.
The data/MC ratio was found to be consistent with one in both the barrel and the endcaps, so there is no systematic uncertainty applied.

\section{Muon RoI cluster trigger, scale factor uncertainty}

The trigger scale factors described in Section~\ref{sec:trigSF} introduce a systematic uncertainty.
To assess these systematic uncertainties, the MS analysis varied the trigger scale factor up and down by the uncertainty of the fit, $\pm 1 \sigma$.
The trigger efficiency was then evaluated using the nominal trigger scale factor, and the trigger scale factor $\pm 1 \sigma$.
The uncertainty was found to be flat vs the LLP radial (longitudinal) decay position in barrel (endcaps), so a flat systematic uncertainty was applied to the trigger efficiency per mass point.
The largest systematic uncertainty due to the trigger scale factors on any mass point, in the barrel or endcaps, was 4.6\%.
These systematic uncertainties are included in the combined systematic uncertainties listed in Table~\ref{table:MSVXsystHiggs} and~\ref{table:MSVXsystPhi}.

\section{Pileup uncertainty}
As discussed in Section~\ref{sec:SimSample_MC}, the pileup distribution in data does not exactly match the pileup modeled in the MC samples, so a PRW is applied to correct the disagreement.
This PRW introduces a source of systematic uncertainty that impacts the trigger and vertex reconstruction efficiency.
The impact from the pileup should already be included in the systematic applied to the IDVX reconstruction described in Section~\ref{sec:syst_VSI_LRT}.
The impact of the PRW on the trigger efficiency and the MSVX reconstruction was evaluated by the MS analysis.
The impact of the systematic uncertainty due to the PRW was determined using the same method to determine the systematic uncertainty due to the trigger scale factors.
The PRW was varied up and down by its uncertainty, and the trigger efficiency and MSVX reconstruction efficiency were compared to the efficiency calculated using the nominal PRW.
The impact was found to be flat compared to the LLP decay position, so a single systematic was applied to the trigger or MSVX reconstruction efficiency in the barrel and endcaps for each mass point.
The largest impact on the trigger efficiency for any mass point in the barrel or endcap was found to be 1\%, and the largest impact on the MSVX reconstruction efficiency was found to be 5.5\%.
These systematic uncertainties are included in the combined uncertainties displayed in Tables~\ref{table:MSVXsystHiggs} and~\ref{table:MSVXsystPhi}.

\section{PDF uncertainty}
The signal MC samples were generated using the parton distribution function (PDF) with QED corrections NNPDF23\_lo\_as-0120\_qed.
A central PDF value is used based on 100 PDF fits, which introduces another source of systematic uncertainty that impacts the trigger efficiency and vertex reconstruction efficiency.
The MS analysis determined these systematic uncertainties by comparing the trigger efficiency and the MSVX reconstruction efficiency using each of the 100 PDF fits compared to the efficiencies obtained using the central value.
The ratio of the efficiencies obtained using the different PDF fits compared to the efficiencies obtained using the central value were found to be approximately flat as a function of the LLP decay position.
A systematic uncertainty was applied to the trigger efficiency and the MSVX reconstruction efficiency in the barrel and the endcaps in each mass point, the largest of these was found to be 1.2\%.
The systematic uncertainties due to the PDF fits are included in the total combined systematic uncertainties in Tables~\ref{table:MSVXsystHiggs} and~\ref{table:MSVXsystPhi}.

The impact of the PDF fits on the IDVX reconstruction efficiency was not evaluated as it would be trivial compared to the uncertainty determined after comparing the $K_S^0$ reconstruction in data and MC samples.

\begin{table}[htb!]
\centering
\begin{tabular}{ c c | c c c c }
	\hline
	\multicolumn{6}{c}{Total Systematic uncertainties} \\
	$m_{\Phi}$	& $m_s$	& Trig. eff (B)			& Trig. eff (E)				& MSVX reco eff (B)			& MSVX reco eff (E)		\\
	\hline
	
	125			& 8 		& $\pm 2.3 \%$ 		& $+4.8,-4.1\%$			& $+0.3,-1.8\%$			& $+0.07,-0.1\%$		\\
	125 			& 15 		& $+2.1,-2.3\%$		& $+4.6,-4.1\%$			& $+5.5,-0.7\%$			&$+0.2,-0.5\%$			\\
	125			& 25 		& $\pm 2.1\%$			& $+3.8,-3.4\%$			& $+0.1,-0.2\%$			&$\pm0.1\%$			\\
	125 			& 40 		& $+1.9,-1.8 \%$ 		& $+3.3,-3.4\%$			& $+0.4,-0.7\%$			&$+0.1,-0.08\%$		\\
	\hline
 \end{tabular}
 \caption{
% 
 Systematic uncertainties associated to the MSVX reconstruction and the muon RoI cluster trigger in the Higgs scalar boson signal samples, resulting from the trigger reconstruction, the pileup and the PDF uncertainties. Listed separately for the barrel (B) and the endcaps (E).
% 
 }
 \label{table:MSVXsystHiggs}
\end{table}

\begin{table}[htb!]
\centering
\begin{tabular}{ c c | c c c c }
	\hline
	\multicolumn{6}{c}{Total Systematic uncertainties} \\
	$m_{\Phi}$	& $m_s$	& Trig. eff (B)			& Trig. eff (E)				& MSVX reco eff (B)			& MSVX reco eff (E)		\\
	\hline
	200			& 8 		& $+2.1,-2.3\%$ 		& $+4.6,-3.8 \%$			& $+5.5,-0.7 \%$			&$+0.2,-0.5\%$			\\
	200			& 25 		&$\pm 2.1\%$			& $+3.8,-3.4 \%$			& $+0.1,-0.2 \%$			&$\pm0.1\%$			\\
	200			& 50 		& $+1.9,-1.8 \%$ 		& $+3,-2.5 \%$				& $+0.4,-0.7 \%$			&$+0.1,-0.08\%$		\\
	\hline
	400			& 50 		&$\pm 1.5,\%$			& $\pm 2.4 \%$				& $+1.2,-0.3 \%$			&$\pm0.2\%$			\\
	400			& 100	&$\pm 1.6\%$			& $+3, -2.5$				& $+0.08,-0.3 \%$			&$\pm0.1\%$			\\
	\hline
	600			& 50 		&$+1.5,-1.4\%$			& $+2.5, -2.2\%$			& $+0.8,-0.2 \%$			&$\pm0.3\%$			\\
	600			& 150	&$+1.3,-1.5\%$			&$\pm 2.1\%$				& $+0.9,-0.3 \%$			&$+0.2,-0.3\%$			\\
	\hline			
	1000			& 50 		&$+1.1,-1.4\%$			& $\pm 1.9\%$				& $+0.05,-0.9 \%$			&$+0.6,-0.8\%$			\\
	1000			& 150	&$\pm 1.1 \%$			& $+1.4,-1.7\%$			& $+0.2,-0.3 \%$			&$+0.3,-0.6\%$			\\
	1000			& 400	&$+1.4,-1.7\%$			& $+2.1,-2.2\%$			& $+0.2,-2.5 \%$	 		&$\pm1.2\%$			\\
	\hline
 \end{tabular}
 \caption{
% 
 Systematic uncertainties associated to the MSVX reconstruction and the muon RoI Cluster trigger in the $\Phi$ scalar boson signal samples, resulting from the trigger reconstruction, the pileup and the PDF uncertainties. Listed separately for the barrel (B) and the endcaps (E).
% 
 }
 \label{table:MSVXsystPhi}
\end{table}