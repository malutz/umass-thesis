\chapter{The ATLAS experiment at the LHC}
\label{sec:exp}

The Large Hadron Collider is located at CERN, just outside of Geneva, Switzerland.
The LHC is the world's largest particle collider, designed to produce high energy proton-proton and heavy ion collisions~\cite{Evans_2008}.
The LHC has two hadron beams circulating in opposite directions, which are designed to collide at four points on the LHC ring where four main experiments are located: ATLAS (A LHC Toroidal ApparatuS), ALICE (A Large Ion Collider Experiment), CMS (Compact Muon Solenoid), and LHCb (Large Hadron Collider beauty).
There have been two LHC running periods thus far, Run 1, which spanned 2010-2012, and Run 2, spanning 2016-2018.
In Run 2 of the LHC, during the data taking used for this analysis, the LHC provided proton-proton collisions with a bunch-spacing of once every 25~ns, and a center of mass energy of 13~TeV.

\section{ATLAS}
The ATLAS detector is a general-purpose particle detector which was designed to probe the proton-proton and ion-ion collisions produced by the LHC~\cite{PERF-2007-01}.
ATLAS is a forward-backward symmetric cylindrical detector and covers a solid angle of nearly 4$\pi$.
In ATLAS a right-handed coordinate system is used, centered on the proton-proton interaction point (IP).
The $(+)x$-axis points towards the center of the LHC ring, the $(+)y$-axis points upwards, and the $z$-axis points along the LHC beamline.
The (+)$z$ side of the ATLAS detector is referred to as side-A while the (-)$z$ side is referred to as side-C.

It is often convenient to refer to a cylindrical coordinate system within the cylindrical detector. 
In this case, the azimuthal angle $\phi$ is defined in the $x$-$y$ plane with $\phi = 0$ along the $x$-axis, and the polar angle $\theta$ is defined from the $z$-axis (along the beamline).
The pseudorapidity is then defined as $\eta = -\ln{\tan{\theta/2}}$.

The ATLAS detector, as shown in Figure~\ref{fig:ATLAS_overview}, is made up of three main sub-detectors, the inner detector, the calorimeters, and the muon spectrometer, and is submerged in a magnetic field provided by toroid and solenoid magnets.

\begin{figure}[!h]
  \includegraphics[width=0.975\textwidth]{Figures/ATLAS_Overview.pdf}
  \centering
  \caption{The ATLAS detector~\cite{PERF-2007-01}. People for scale.}
  %If this plot is kept/remade - add R [mm] label to the x-axis, fix the labeling of the sample, and add a dashed line representing the beampipe, which is corresponding to the bump just after 20mm.
  \label{fig:ATLAS_overview}
\end{figure} 

%Is it worth mentioning non-detector materials which can impact eventual fiducial volume?
%Beamline \\

\subsection{Inner Detector}

The inner detector is used primarily for the tracking of charged particles.
The ID consists of three sub-systems, the silicon pixel and silicon micro-strip (SCT) detectors, and the Transition Radiation Tracker (TRT), which are immersed in a 2 T magnetic field provided by the solenoid magnet~\cite{PERF-2007-01}.
The pixel detector has four cylindrical layers in the barrel (including the Insertable $B$-Layer (IBL) which was included after the end of Run 1) and three endcap disks on each side, as shown in Figure~\ref{fig:ID_overview}~\cite{ID_RUN2}.
The SCT likewise has 4 cylindrical layers in the barrel, and 9 disks in each endcap.
The pixel and SCT detector provide precision tracking for $|\eta| < 2.5$.
The pixel barrel (endcaps) provides coverage for $R < 122.5$~mm ($R <149.6$~mm) and $|z| < 400.5$~mm ($495 < |z| < 650$~mm), and the SCT barrel (endcaps) provides coverage for $299 < R < 514$~mm ($275 < R < 560$~mm) and  $|z| < 749$~mm ($839 < |z| < 2,735$~mm).
The layers of the pixel detector are segmented in $R-\phi$ and $z$, with a minimum pixel size of $50 \times 400 \, \mu\mathrm{m}^2$ in $R-\phi \times z$ and intrinsic  accuracies of 10 $\mu$m in $R-\phi$ and 115 $\mu$m in $z$ ($R$) in the barrel (endcaps).
The exception to this is the IBL, which is more finely segmented than the rest of the pixel layers with a pixel size of $50 \times 250 \, \mu\mathrm{m}^2$ in $R-\phi \times z$~\cite{ID_RUN2}.

\begin{figure}[!h]
  \includegraphics[width=0.975\textwidth]{Figures/ATLAS_ID_RUN2_fig_01.pdf}
  \centering
  \caption{The $R-z$ cross-section of the ID of the ATLAS detector~\cite{ID_RUN2}. 
  The lower-lefthand inset shows an enlarged version of the barrel of the pixel detector, and the lower-righthand inset displays a table describing the radial coverage of the three ID sub-systems. }
  \label{fig:ID_overview}
\end{figure} 

The layers in the SCT consist of micro-strip modules.
Each module has two micro-strips with a stereo-angle of 40 mrad to provide space points with $R, \, z, \, \mathrm{and} \, \phi$ information.
The intrinsic accuracies of these modules in the barrel (endcaps) are 17 $\mu$m ($R-\phi$) and 580 $\mu$m ($z$) (17 $\mu$m ($R-\phi$) and 580 $\mu$m ($R$)).

The TRT is located outside of the pixel and SCT detectors (Figure~\ref{fig:ID_overview}).
It consists of layers of 4~mm diameter straw tubes which provide high precision momentum measurements and electron identification for tracks in a region $|\eta| < 2.0$.
The straw tubes are 144~cm long in the barrel and 37~mm long in the endcaps, and thus only provide 2 coordinates per hit ($R-\phi$ in the barrel and $z-\phi$ in the endcaps).
Due to the fact the TRT provides only two coordinates per hit, it is difficult to seed tracks for track reconstruction (explained in Section~\ref{sec:recoST}), however the TRT provides an average of 36 additional hits per track to the tracks which are seeded in the silicon sub-systems.

\subsection{Calorimeters}

Calorimetry is provided by liquid argon (LAr) electromagnetic and scintillator-tile and LAr hadronic calorimeters~\cite{PERF-2007-01}.
The electromagnetic calorimeter (ECal) covers the region $|\eta| < 1.475$ in the barrel and $1.375 < |\eta| < 3.2$ in the endcaps, with the ECal barrel divided into two by a 4~mm wide gap at $z=0$ and the ECal endcaps divided into inner and outer wheels covering the regions $1.375 < |\eta| < 2.5$ and $2.5 < |\eta| < 3.2$ respectively.
The ECal has an accordion shaped geometry, designed to prevent cracks in $\phi$ coverage.
An active LAr pre-sampler detector covers the region $|\eta| < 1.8$ to correct for energy lost to upstream electrons and photons.

The HCal is located just outside of the ECal. 
A tile calorimeter barrel provides hadronic calorimetry for $|\eta| < 1.0$ and an extended tile barrel covers $0.8 < |\eta| < 1.7$.
The HCal is a sampling calorimeter which uses a steel absorber in addition to the active scintillating tiles.

Like the ECal, the HCal uses LAr cryostats in the endcaps. 
A Hadronic Endcap Calorimeter, is found just outside of the ECal in $|z|$ with two wheels of wedge-shaped modules providing coverage from $|\eta| = 1.5$ to $|\eta| = 3.2$~\cite{PERF-2007-01}, and a Forward LAr endcap extending the pseudorapidity coverage of the calorimeters out to $|\eta| < 4.9$.


\subsection{Muon Spectrometer}
%Magnets ? 
The muon spectrometer consists of four sub-systems, which are shown in Figure~\ref{fig:MS_overview}.
Precision tracking is provided in the barrel and endcaps by the Monitored Drift Tubes (MDTs) up to $|\eta| < 2.7$, and in the endcaps by the Cathode Strip Chambers (CSCs) in a range $2.0 < |\eta| < 2.7$.
Triggering and additional tracking information is provided in the barrel up to $|\eta| < 1.05$ by the Resistive Plate Chambers (RPCs) and by the Thin Gap Chambers (TGCs) in the endcaps for $1.05 < |\eta| < 2.7$ (the triggering capabilities extend to $|\eta| < 2.4$)~\cite{PERF-2007-01}. 

\begin{figure}[!h]
  \centering
  	\includegraphics[width=0.975\textwidth]{Figures/MSVXRECO_fig_01.pdf}
    \caption{
  %Test
  The $R-z$ cross-section of the MS of the ATLAS detector~\cite{PERF-2013-01}. 
  The MDTs are shown as green (cyan) blocks in the barrel (endcaps), the CSCs are represented in yellow, the TGCs in purple, and the RPCs as black lines.
The dashed blue lines represent possible paths of high energy $\mu$'s through the MS.
 }
  \label{fig:MS_overview}
\end{figure} 

The MDTs in the barrel are arranged in three cylindrical layers, at radii of 5~m, 7.5~m, and 10.0~m (like in the calorimeter, there is a small gap at $|z| = 0$), and in three endcap layers on each side at $|z|$ positions of 7.4~m, 14.0~m and 21.5~m~\cite{PERF-2007-01}.
The CSCs are located in the endcaps at $|z|$ positions at 7.4~m.
The MDTs in the barrel are submerged in a magnetic field providing 1.5-5.5~Tm of bending power, whereas the MDTs located in the endcaps are all outside of the magnetic field regions provided by the endcap toroids~\cite{PERF-2013-01}. 
The tubes in the MDTs are arranged in two multi-layers (MLs), with 3-4 layers of drift tubes each.
These tubes are 30~mm in diameter and can measure the drift radius of a charged particle with a resolution of 80 $\mu$m, but are 2-5~m long and thus can only provide very rough measurements in $\phi$.
The $\phi$ coordinates are therefore taken from measurements in the RPCs and TGCs.

In order to provide triggering information, the RPCs are arranged in three layers in the MS barrel at 7.820~m, 8.365~m, and 10.229~m.
Each layer of RPCs has two detector layers, each providing $\eta$ and $\phi$ measurements (meaning there are two measurements of $\eta$ and $\phi$ provided for each RPC layer a charged particle passes through).
The large gap between the middle and outer layers facilitates triggering on charged particles with high transverse momentum (\pt), of $9 <$ \pt~$< 35$ GeV, and the smaller gap between the inner and middle layers allows for a low-\pt~trigger.

In the endcaps, the TGCs are arranged in 9 total layers on each side, a doublet layer just before the innermost MDT layer, and two doublets and a triplet surrounding the central MDT layer (see Figure~\ref{fig:MS_overview}).
The TGC chambers are equipped with anode wires and cathode strips which provide precision $\eta$ and $\phi$ measurements respectively.
The cathode strips are arranged to have an azimuthal granularity of 2-3~mrad, and the anode wires to have a wire-to-wire distance of 1.8~mm, in order to have a sufficient granularity for the required momentum resolution of the triggers.


\subsection{Trigger System}

In Run 2 of the LHC, the ATLAS experiment made use of a two-level trigger system, with a level-1 (L1) hardware trigger and a high level software trigger (HLT), which reduced the event collision rate of 40~MHz to 100~kHz and then to approximately 1~kHz~\cite{TRIG-2016-01}, demonstrated schematically in Figure~\ref{fig:ATLAStrig_schem}.

\begin{figure}[!h]
  \centering
  	\includegraphics[width=0.975\textwidth]{Figures/Run2Trig_fig_01.pdf}
    \caption{
A schematic representation of the ATLAS L1 and HLT trigger system~\cite{TRIG-2016-01}. 
The FTK shown here is still being commissioned and was not employed during 2016 data taking. }
  \label{fig:ATLAStrig_schem}
\end{figure} 

The L1 trigger is based on hardware, taking inputs from the calorimeters and MS, as well as the Minimum Bias Trigger Scintillators~\cite{Sidoti_2014}, the LUCID detector~\cite{Avoni_2018}, and the Zero-Degree Calorimeter~\cite{Leite:2013qxa}.
A Central Trigger Processor (CTP) takes the information from these sub-detectors and selects events based on an input trigger menu of different trigger options.
The triggers in this menu may be pre-scaled, in other words to reduce the rate of triggers which may arrive very frequently, the CTP can be told to only select a certain fraction of events passing a given trigger.
The L1 trigger will identify and pass along information about Regions-of-Interest (RoIs) which are $\eta-\phi$ regions around interesting features in the event such as high energy $\mu$'s, $\gamma$'s, etc.
The L1 trigger can either apply a simple or complex dead-time to reduce the trigger rates.
A simple dead-time enforces a minimum number of non-accepted events after each L1 trigger, while the complex dead-time restricts the number of events passing the L1 trigger over the course of a given number of total bunch-crossings.

The HLT is a software-based trigger which takes as input the event and RoI information from L1 and uses it to perform limited object reconstruction.
The HLT uses a more complex trigger menu to apply a final selection to events, which are then exported for permanent storage and offline reconstruction.
Pre-scaling may also be applied to triggers at the HLT.

\begin{comment}
\subsection{LUCID}

The forward detector, LUCID (LUminosity measurement using Cerenkov Integrating Detector), is designed for online monitoring of the instantaneous luminosity and beam conditions delivered by the LHC~\cite{PERF-2007-01}. 
The LUCID detectors are located at $|z| = 17$~m on either side of the IP, in order to detect inelastic p-p scattering, operating under the assumption that the number of interactions per bunch crossing is proportional to the number of particles measured by these detectors.
\end{comment}

