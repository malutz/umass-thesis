\chapter{Results}
\label{sec:res}

\section{Global Efficiency}
It is computationally forbidding to produce a statistically useful number of events at a large range of lifetime points, therefore in order to understand the event selection efficiency and the limits on a given mass and cross-section across a range of lifetimes, an extrapolation method must be used.

\subsection{Lifetime Extrapolation}
\label{sec:ctauextrap}
To evaluate the efficiency of the full event selection as a function of the proper lifetime of the LLP, a re-weighting procedure is used.
 Each LLP in each event is given a weight according to Eq.~\ref{eq:extrapweight},
  \begin{equation}
 w_l(t) = \frac{\tau_{gen}}{\exp(\frac{-t_l}{\tau_{gen}})} \cdot \frac{\exp(\frac{-t_l}{\tau_{new}})}{\tau_{new}},
 \label{eq:extrapweight}
 \end{equation}
in which $\tau_{gen}$ is the lifetime of the signal MC sample,  $\tau_{new}$  is an arbitrary lifetime point, and $t$ is the proper decay time, based on the three-dimensional decay position, the $\beta$, and the $\gamma$ of the LLP.
 
Since the LLPs are pair-produced, the weight for each event becomes $w_{ev} = w_1 \cdot w_2$, and the overall efficiency to select an event at any given lifetime is 
 \begin{equation}
 \epsilon = \frac{\Sigma_{ev} \mathrm{event\,weight}_{ev}\times\mathrm{pass}_{ev}\times w_{ev} \times SF_{trig}}{\Sigma_{all}},
 \label{eq:globaleff}
 \end{equation}
where the term $SF_{trig}$ takes into account the data/MC trigger scale factor depending on whether the MSVX was in the barrel or the endcap of the MS, and `event weight' takes into account weighting to make the MC events match the data events (such as the PRW).
The results of the lifetime extrapolation starting from a mean lab-frame lifetime of 5 m are shown in Figure~\ref{fig:GlobalEff}. 

 \begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:globalEffLow}\includegraphics[width=0.5\textwidth]{Figures/GlobalEff_lowMass.pdf}}
	\subfloat[]{\label{subfig:globalEffHigh}\includegraphics[width=0.5\textwidth]{Figures/GlobalEff_highMass.pdf}}
	\caption{Global efficiency vs c$\tau$ [m] for Higgs or $\Phi = 200 \, \mathrm{GeV} \rightarrow ss$ (a) and for $\Phi = 400, \, 600, \,1000 \, \mathrm{GeV} \, \rightarrow ss$ (b).}
	\label{fig:GlobalEff}
\end{figure}

Confirmation of the effectiveness of the lifetime extrapolation method was evaluated by comparing the extrapolated efficiency from the 5~m mean lab-frame lifetime sample to the efficiency found in the 9~m mean lab-frame lifetime sample (at the nominal lifetime), as well as by comparing the lifetime extrapolation curves derived from both the 5~m lab-frame lifetime and 9~m lab-frame lifetime samples.
Comparisons of the lifetime extrapolations from the 5~m an 9~m mean lab-frame lifetime samples are shown in Figure~\ref{fig:GlobalEffCompare}.

 \begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:globalEff125_8}\includegraphics[width=0.5\textwidth]{Figures/effWithRatio_mH125mS8.pdf}}
	\subfloat[]{\label{subfig:globalEff125_40}\includegraphics[width=0.5\textwidth]{Figures/effWithRatio_mH125mS40.pdf}}
	\\
	\subfloat[]{\label{subfig:globalEff600_50}\includegraphics[width=0.5\textwidth]{Figures/effWithRatio_mH600mS50.pdf}}
	\subfloat[]{\label{subfig:globalEff1000_150}\includegraphics[width=0.5\textwidth]{Figures/effWithRatio_mH1000mS150.pdf}}
	\caption{
	The comparison of the lifetime extrapolation starting from the 5~m lab-frame lifetime sample (red), and starting from the 9~m lab-frame lifetime sample (blue). The efficiency at the mean generated lifetimes for each sample are included as the red (blue) points. The comparison is shown for mass points with a 125~GeV Higgs decaying to an 8~GeV (a) or 40~GeV LLP (b), for a 600~GeV $\Phi$ to a 50~GeV LLP (c), and for a 1000~GeV $\Phi$ to a 150~GeV LLP (d)
	Uncertainties are statistical only.
	}
	\label{fig:GlobalEffCompare}
\end{figure}

Figure~\ref{fig:GlobalEffCompare} demonstrates agreement of the lifetime extrapolation curves from the 5~m and 9~m lab-frame lifetime samples over a wide range of the extrapolated c$\tau$.
The extrapolated efficiency generally agrees within the statistical uncertainties.
The statistical uncertainties are fairly large, particularly for the mass points with the lowest LLP masses, due to the limitations of the IDVX reconstruction and the requirement that the IDVX mass must be $> 3$~GeV.
There is some further disagreement in the lifetime extrapolation curves at low c$\tau$; it is expected that the extrapolation from the 9~m mean lab-frame lifetime will do a worse job at very low c$\tau$ due to the fact the low decay length regions will be less populated for these samples and the fact that the extrapolation is further from the nominal lifetime of the sample.

At the c$\tau$ of the 9~m mean lab-frame lifetime point, the extrapolated efficiency from the 5~m mean lab-frame lifetime samples agreed with the nominal efficiency found in the 9~m mean lab-frame lifetime samples within at most 1.3 times the combined statistical uncertainties from the efficiency and extrapolation computations.

\section{Observed signal region events}
One event was observed in the signal region.
The observed event is visualized in Figure~\ref{fig:vp1}.

 \begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:vp1_zoom}\includegraphics[width=0.75\textwidth]{Figures/vp1_beampipeview.png}}
	\\
	\subfloat[]{\label{subfig:vp1_all}\includegraphics[width=0.75\textwidth]{Figures/vp1_outsideview.png}}
	\caption{
The event observed in the signal region, Event number 1804273557 in Run 303338. The IDVX is shown as a dark blue sphere and the associated tracks are shown in light blue. The MSVX is shown in purple, and the PV is shown in green. The event is shown in (a) zoomed in from the endcaps and in (b), zoomed out from outside the barrel. The IDVX has 4 associated tracks, a mass of 3.34~GeV, and is located at an (R,z) of (29.07,142.92)~mm and detector ($\eta$,$\phi$) of (2.30,0.014). The MSVX is located at an (R,z) of (4.97,12.45)~m and a detector ($\eta$,$\phi$) of (1.65,-2.94).
	}
	\label{fig:vp1}
\end{figure}

Given that the background estimate, described in Section~\ref{sec:bgr}, predicted $ 1.2 \pm 0.2 \,(stat) \pm 0.56\,(syst.)$ events from background to be in the signal region, this observation is consistent with background.
Since no excess is observed, limits are set on the production cross section of the $\Phi$ times the branching ratio ($BR$) for the decay of the $\Phi$ to the LLPs.


\section{Limits}
\label{sec:limits}

\subsection{Limit setting procedure}
\label{sec:limits_IDMS}
Upper limits are set at a 95\% confidence level (CL) using the \CLs~method~\cite{Read:2002hq,Gross:1099994}.
The \CLs~method is a popular method of setting limits in particle physics designed to take into account the two different goals of particle physics experiments, exclusion and discovery.

A test statistic can be developed which is the ratio of the likelihoods for the exclusion and discovery hypotheses.
The denominator is the null, or background only, hypothesis, that the data can be explained using only existing physics.
The numerator is the alternative, or signal plus background hypothesis, that to explain the data necessitates new physics.

The \CLs~method can be considered as $P(n_{s+b} \leq n_o)/P(n_b \leq n_o)$, the probability that the number of observed events is $\leq$ the number of events predicted from background and signal processes combined, over the probability that the number of observed events is $\leq$ the number of events from background processes only.
This is typically written out as \CLs~$= CL_{s+b}/CL_{b}$.

For this analysis, the calculation of the test statistic was done using pseudo-experiments and a Poisson probability term is used to describe the number of observed events\footnote{The limits studies were performed by Jackson Burzynski.}.
The systematic uncertainties on the signal efficiency, estimated number of background events, and luminosity, are treated as nuisance parameters with assigned Gaussian constraints.
The limit is extrapolated to each c$\tau$ point using the lifetime extrapolation method described in Section~\ref{sec:ctauextrap}.
%CLs - popular method to set limits at a 95 \% confidence limit, taking into account a background only hypothesis and a signal+background hypothesis, $CL_{s+b}/CL_{b}$. \\
%There was no excess, the number of events in the signal region was equal to the number of predicted background events, thus limits were set, and thus the actual limits were equal to the predicted limits. 
%Systematic uncertainties - nuisance parameters.
%Profile likelihood function used as a test statistic, toy MC used.

\subsection{Limits from this analysis}
Limits are set on all mass points listed in Table~\ref{table:HSSall}.
Limits on the SM Higgs decaying to LLPs are placed on the $BR$ to the LLPs, assuming the gluon-gluon fusion production cross-section for the Higgs, $\sigma_{\mathrm{ggF}} = 48.58$~pb~\cite{deFlorian:2016spz}.
These limits are shown including $\pm 1\sigma$ (green) and $\pm 2\sigma$ (yellow) error bands in Figure~\ref{fig:BrazilLimits_Higgs}, and are summarized in Figure~\ref{fig:Limits_AllHiggs}.
The expected and observed limits are both displayed in Figure~\ref{fig:BrazilLimits_Higgs}, but overlap very closely due to the agreement between the expected background and the observed number of events in the signal region.

 \begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:brazilLimit_mH125mS8}\includegraphics[width=0.485\textwidth]{Figures/mH125mS8Brazil.pdf}}
	\\
	\subfloat[]{\label{subfig:brazilLimit_mH125mS15}\includegraphics[width=0.485\textwidth]{Figures/mH125mS15Brazil.pdf}}
	\subfloat[]{\label{subfig:brazilLimit_mH125mS25}\includegraphics[width=0.485\textwidth]{Figures/mH125mS25Brazil.pdf}}
	\\
	\subfloat[]{\label{subfig:brazilLimit_mH125mS40}\includegraphics[width=0.485\textwidth]{Figures/mH125mS40Brazil.pdf}}
	\subfloat[]{\label{subfig:brazilLimit_mH125mS55}\includegraphics[width=0.485\textwidth]{Figures/mH125mS55Brazil.pdf}}
	\caption{
	$\mathrm{CL}_{\mathrm{s}}$ limits on $BR$ for Higgs$\rightarrow$ss, assuming a $\sigma$ for the Higgs equal to that of the SM Higgs produced via ggF. 
	The limits on the $BR$ are shown for the Higgs decaying to an (a) 8~GeV LLP, a (b) 15~GeV LLP, a (c) 25~GeV LLP, a (d) 40~GeV LLP, and an (e) 55~GeV LLP.
	The green and yellow bands represent the $\pm1\sigma$ and $\pm2\sigma$ error bands.
	\textit{Figures were created by J. Burzynski.}
	}
	\label{fig:BrazilLimits_Higgs}
\end{figure}

 \begin{figure}[!htb]
	\centering
 	\includegraphics[width=0.7\textwidth]{Figures/mH125Combined.pdf}
	\caption{
	$\mathrm{CL}_{\mathrm{s}}$ limits on $BR$ for $H\rightarrow ss$, assuming a SM Higgs produced via ggF.
	\textit{Figure was created by J. Burzynski.}
	}
	\label{fig:Limits_AllHiggs}
\end{figure}

There are two LLP mass points studied here that are identical to those studied in the Run 1 version of this search.
The excluded c$\tau$ [m] ranges for the Higgs to LLPs at a 15\% $BR$ are shown for this search compared to the Run 1 search for a Higgs decaying to hidden valley pions with a topology of one decay in the ID and one in the MS or two decays in the MS.
This search alone slightly extends the limits on the $BR$ to a lower c$\tau$.
The excluded range presented here does not extend to as high a c$\tau$ as those presented in Run 1 due to the inclusion of the 2-MSVX topology in the Run 1 results.

\begin{table}[!htb]
  \centering
  \begin{tabular}{ c  c  c } 
    \hline
    \multicolumn{3}{c}{Excluded c$\tau$ range [m]}	\\
    $m_s$ [GeV]	&	Run 1	&	Run 2 			\\
     	& \multicolumn{2}{c}{15\% $BR$}		\\
    \hline
    25 	&   0.28-32.8	& 	0.11-7.46			\\
    40 	&   0.68-55.5	& 	0.22-12.83		\\
   \hline
  \end{tabular}
  \caption{Excluded c$\tau$ [m] ranges for the Higgs to LLPs at a 15\% $BR$, from the Run 1 search for a Higgs to hidden valley scalar decaying in the ID and MS or MS, compared to those set by this search alone.
  }
  \label{table:Run1vRun2_HiggsBRLimits}
\end{table}

Limits on the $\sigma \times BR$ are shown for a 200~GeV $\Phi$ in Figure~\ref{fig:BrazilLimits200} and for a 400~GeV $\Phi$ in Figure~\ref{fig:BrazilLimits400}, and are summarized in Figure~\ref{fig:Limits200_400}.

 \begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:brazilLimit_mH200mS8}\includegraphics[width=0.485\textwidth]{Figures/mH200mS8Brazil.pdf}}
	\\
	\subfloat[]{\label{subfig:brazilLimit_mH200mS25}\includegraphics[width=0.485\textwidth]{Figures/mH200mS25Brazil.pdf}}
	\,
	\subfloat[]{\label{subfig:brazilLimit_mH200mS50}\includegraphics[width=0.485\textwidth]{Figures/mH200mS50Brazil.pdf}}
	\caption{$\mathrm{CL}_{\mathrm{s}}$ limits on $\sigma \times BR$ for 200~GeV $\Phi\rightarrow ss$.
	The limits on the $\sigma \times BR$ are shown for the 200~GeV $\Phi$ decaying to an (a) 8~GeV LLP, a (b) 25~GeV LLP, and a (c) 50~GeV LLP.
	The green and yellow bands represent the $\pm1\sigma$ and $\pm2\sigma$ error bands.
	\textit{Figures were created by J. Burzynski.}
	}
	\label{fig:BrazilLimits200}
\end{figure}

\begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:brazilLimit_mH400mS50}\includegraphics[width=0.495\textwidth]{Figures/mH400mS50Brazil.pdf}} 
	\subfloat[]{\label{subfig:brazilLimit_mH400mS100}\includegraphics[width=0.495\textwidth]{Figures/mH400mS100Brazil.pdf}}
	\caption{$\mathrm{CL}_{\mathrm{s}}$ limits on $\sigma \times BR$ for 400 GeV~$\Phi\rightarrow ss$.
	The limits on the $\sigma \times BR$ are shown for the 400~GeV $\Phi$ decaying to an (a) 50~GeV LLP and a (b) 100~GeV LLP.
	The green and yellow bands represent the $\pm1\sigma$ and $\pm2\sigma$ error bands.
	\textit{Figures were created by J. Burzynski.}
	}
	\label{fig:BrazilLimits400}
\end{figure}
 
 \begin{figure}[!htb]
	\centering
 	\includegraphics[width=0.7\textwidth]{Figures/mH200-400Combined.pdf}
	\caption{	
	$\mathrm{CL}_{\mathrm{s}}$ limits on $\sigma \times BR$ for 200 and 400~GeV~$\Phi \rightarrow ss$.
	\textit{Figure was created by J. Burzynski.}
	}
	\label{fig:Limits200_400}
\end{figure}

Limits on the $\sigma \times BR$ are shown for a 600~GeV $\Phi$ in Figure~\ref{fig:BrazilLimits600} and for a 1000~GeV $\Phi$ in Figure~\ref{fig:BrazilLimits1000}, and are summarized in Figure~\ref{fig:Limits600_1000}.


\begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:brazilLimit_mH600mS50}\includegraphics[width=0.495\textwidth]{Figures/mH600mS50Brazil.pdf}} 
	\subfloat[]{\label{subfig:brazilLimit_mH600mS150}\includegraphics[width=0.495\textwidth]{Figures/mH600mS150Brazil.pdf}}
	\caption{$\mathrm{CL}_{\mathrm{s}}$ limits on $\sigma \times BR$ for 400 GeV~$\Phi\rightarrow ss$.
	The limits on the $\sigma \times BR$ are shown for the 600~GeV $\Phi$ decaying to an (a) 50~GeV LLP and a (b) 150~GeV LLP.
	The green and yellow bands represent the $\pm1\sigma$ and $\pm2\sigma$ error bands.
	\textit{Figures were created by J. Burzynski.}
	}
	\label{fig:BrazilLimits600}
\end{figure}

 \begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:brazilLimit_mH1000mS50}\includegraphics[width=0.485\textwidth]{Figures/mH1000mS50Brazil.pdf}}
	\\
	\subfloat[]{\label{subfig:brazilLimit_mH1000mS150}\includegraphics[width=0.485\textwidth]{Figures/mH1000mS150Brazil.pdf}}
	\subfloat[]{\label{subfig:brazilLimit_mH1000mS400}\includegraphics[width=0.485\textwidth]{Figures/mH1000mS400Brazil.pdf}}
	\caption{$\mathrm{CL}_{\mathrm{s}}$ limits on $\sigma \times BR$ for 200~GeV $\Phi\rightarrow ss$.
	The limits on the $\sigma \times BR$ are shown for the 200~GeV $\Phi$ decaying to a (a) 50~GeV LLP, a (b) 150~GeV LLP, and a (c) 400~GeV LLP.
	The green and yellow bands represent the $\pm1\sigma$ and $\pm2\sigma$ error bands.
	\textit{Figures were created by J. Burzynski.}
	}
	\label{fig:BrazilLimits1000}
\end{figure}
 
 \begin{figure}[!htb]
	\centering
 	\includegraphics[width=0.7\textwidth]{Figures/mH600-1000Combined.pdf}
	\caption{	
	$\mathrm{CL}_{\mathrm{s}}$ limits on $\sigma \times BR$ for 600 and 1000~GeV~$\Phi \rightarrow ss$.
	\textit{Figure was created by J. Burzynski.}
	}
	\label{fig:Limits600_1000}
\end{figure}

The only overlapping $\Phi\rightarrow ss$ mass points that are common between this analysis and the Run 1 search are the $m_{\Phi} = 600$~GeV mass points.
For these two mass points, the excluded region for $\sigma \times BR \leq 1$ is extended to a slightly lower c$\tau$ in the limits from this analysis alone, than that presented in the Run 1 search.

\subsection{Combined limits}
\label{sec:limits_combo}
The limits from this search alone can be combined with the limits presented in searches for a Higgs or heavy scalar $\Phi$ decaying to neutral scalars, which produced decay vertices in the HCal~\cite{CalRatio2016} or in the MS~\cite{EXOT-2017-05}.
The event selection used in this search was designed to be explicitly orthogonal to that used by the search for displaced hadronic jets in the HCal, by placing a veto on the triggers used to define the signal region in that analysis (as described in Section~\ref{sec:trig}).
The search for displaced hadronic jets decaying in the MS was broken into two topologies, 2 MSVXs (MS2) or one MSVX as well as missing transverse energy ($E_{\mathrm{T}}$) in the event (MS1).
This analysis is approximately orthogonal to the MS2 topology, as only a few events in two of the signal MC samples contained 2 good MSVXs as well as a good IDVX (and the removal of these events has no impact on the combined limits), however there is a significant overlap in the events selected in the signal region of the MS1 topology and in the signal region presented here.
Thus limits from the CR analysis and the limits from the MS2 topology from the MS analysis can be combined with the limits presented in Section~\ref{sec:limits_IDMS}, but the limits from the MS1 topology are excluded.

The limits are combined using a simultaneous fit of the profile likelihood functions from each of the individual analyses.
The signal strength and the nuisance parameter associated with the systematic uncertainty in the integrated luminosity are chosen to be the same for each, while the signal uncertainties are dominated by different sources in each search and are thus chosen to be uncorrelated.
The background estimations used by all of the searches are data-driven and thus also uncorrelated.
The limits are calculated using a global fit in which the combined profile likelihood function is the product of each individual likelihood function.

The limits on the $BR$ of a SM Higgs to Hidden Sector LLPs set by this search (as presented in Figures~\ref{fig:BrazilLimits_Higgs} and Figure~\ref{fig:Limits_AllHiggs}), are shown versus the combined limits from the CR and MS analyses, and compared to the combined limits from this search, the CR analysis, and the MS2 topology from the MS analysis in Figure~\ref{fig:CompareLimits_Higgs}.
The expected and observed limits are shown for the individual and the combined limits, and the shaded areas represent $\pm 1 \sigma$ error bands.
The limits on the branching ratio for a Higgs decaying to a 55~GeV LLP are shown for the CR analysis only instead of the CR and MS analyses because the MS analysis did not place limits on the $BR$ for this mass point. 

 \begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:compareLimit_mH125mS8}\includegraphics[width=0.485\textwidth]{Figures/mH125mS8Compare.pdf}}
	\subfloat[]{\label{subfig:compareLimit_mH125mS15}\includegraphics[width=0.485\textwidth]{Figures/mH125mS15Compare.pdf}}
 \\
	\subfloat[]{\label{subfig:compareLimit_mH125mS25}\includegraphics[width=0.485\textwidth]{Figures/mH125mS25Compare.pdf}}
	\subfloat[]{\label{subfig:compareLimit_mH125mS40}\includegraphics[width=0.485\textwidth]{Figures/mH125mS40Compare.pdf}}
	\\
	\subfloat[]{\label{subfig:compareLimit_mH125mS55}\includegraphics[width=0.485\textwidth]{Figures/mH125mS55CompareCR.pdf}}
	\caption{ $CL_S$ limits on $BR$ for $H \rightarrow ss$, assuming a $\sigma$ for the Higgs equal to that of the SM Higgs produced via ggF.
	The limits are shown for the Higgs decaying to an (a) 8~GeV LLP, a (b) 15~GeV LLP, a (c) 25~GeV LLP, a (d) 40~GeV LLP, and an (e) 55~GeV LLP.
	The limits set by this analysis (green) are compared with those set by the CR+(MS1+MS2) analyses (purple), as well as the combination of all three analyses (blue).
	\textit{Figures were created by J. Burzynski.}
	}
	\label{fig:CompareLimits_Higgs}
\end{figure}

At low c$\tau$, the limits set by this search outperform the combined limits from the CR and MS analyses very slightly for the Higgs decay to a 8 or 15~GeV LLP (Figures~\ref{subfig:compareLimit_mH125mS8} and~\ref{subfig:compareLimit_mH125mS15}), and more significantly for decays of the Higgs to 25~GeV (Figure~\ref{subfig:compareLimit_mH125mS15}), 40~GeV (Figure~\ref{subfig:compareLimit_mH125mS15}), and 55~GeV (Figure~\ref{subfig:compareLimit_mH125mS15}) LLPs.
Thus, the combined ID+CR+MS2 limits represent the strongest limits yet set on the decay of a SM Higgs to neutral LLPs, at these mass points, which hadronically decay back to SM fermions.

The inclusion of the results from this analysis do not strengthen the combined limits significantly for the decays of the SM Higgs to an 8 or 15~GeV LLP due to the limitations in the IDVX reconstruction and selection efficiency for lower mass LLPs. 
The selection on the LLP mass in particular significantly restricts the IDVX reconstruction efficiency for the 8~GeV LLP due to the similarity in the vertex mass distributions for the reconstructed vertices matched to 8~GeV LLPs and the IDVXs found in background (see Figure~\ref{subfig:SvBmass_mH125}).
The inclusion of the results from this analysis for the Higgs decay to LLPs with mass $\geq$ 25~GeV strengthen the combined limits more notably, due to the increased IDVX reconstruction efficiency with increased LLP mass for a constant $\Phi$, allowing the IDVX+MSVX topology to make a bigger improvement over the limits set by the MS1 topology at low c$\tau$.

The combined limits on the $\sigma \times BR$ for a 200~GeV $\Phi$ and a 400~GeV $\Phi$ are shown in Figure~\ref{fig:CompareLimits_200_400} and for a 600~GeV $\Phi$ and a 1000~GeV $\Phi$ are shown in Figure~\ref{fig:CompareLimits_600_1000}.


 \begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:compareLimit_mH200mS8}\includegraphics[width=0.485\textwidth]{Figures/mH200mS8Compare.pdf}}
	\\
	\subfloat[]{\label{subfig:compareLimit_mH200mS25}\includegraphics[width=0.485\textwidth]{Figures/mH200mS25Compare.pdf}}
	\subfloat[]{\label{subfig:compareLimit_mH200mS50}\includegraphics[width=0.485\textwidth]{Figures/mH200mS50Compare.pdf}}
	\\
	\subfloat[]{\label{subfig:compareLimit_mH400mS50}\includegraphics[width=0.485\textwidth]{Figures/mH400mS50Compare.pdf}}
	\subfloat[]{\label{subfig:compareLimit_mH400mS100}\includegraphics[width=0.485\textwidth]{Figures/mH400mS100Compare.pdf}}
	\caption{ $CL_S$ limits on $\sigma \times BR$ for a 200~GeV $\Phi$ decaying to an (a) 8~GeV LLP, (b) 25~GeV LLP, and a (c) 50~GeV LLP, and for a  400~GeV $\Phi$ decaying to a (d) 50~GeV LLP and an (e) 100~GeV LLP.
	The limits set by this analysis (green) are compared with those set by the CR+(MS1+MS2) analyses (purple), as well as the combination of all three analyses (blue).
	\textit{Figures were created by J. Burzynski.}
	}
	\label{fig:CompareLimits_200_400}
\end{figure}

Once again, limitations of the IDVX selection for low mass LLPs limited the impact of the inclusion of the results from this analysis on the limits for the $\sigma \times BR$ for a 200~GeV $\Phi$ to an 8~GeV LLP.
The results from this analysis however strengthened the limits significantly for a 200~GeV $\Phi$ to 25 or 50~GeV scalars compared to the combined limits from the CR and MS analyses at low c$\tau$.


 \begin{figure}[!htb]
	\centering
	\subfloat[]{\label{subfig:compareLimit_mH600mS50}\includegraphics[width=0.485\textwidth]{Figures/mH600mS50Compare.pdf}}
	\subfloat[]{\label{subfig:compareLimit_mH600mS150}\includegraphics[width=0.485\textwidth]{Figures/mH600mS150Compare.pdf}}
	\\
	\subfloat[]{\label{subfig:compareLimit_m1000mS50}\includegraphics[width=0.485\textwidth]{Figures/mH1000mS50Compare.pdf}}
	\subfloat[]{\label{subfig:compareLimit_mH1000mS150}\includegraphics[width=0.485\textwidth]{Figures/mH1000mS150Compare.pdf}}
	\\
	\subfloat[]{\label{subfig:compareLimit_mH1000mS400}\includegraphics[width=0.485\textwidth]{Figures/mH1000mS400Compare.pdf}}
	\caption{ $CL_S$ limits on $\sigma \times BR$ for a 600~GeV $\Phi$ decaying to an (a) 50~GeV LLP, (b) 150~GeV LLP, and for a 1000~GeV $\Phi$ decaying to a (c) 50~GeV LLP, a (d) 150~GeV LLP, and an (e) 100~GeV LLP.
	The limits set by this analysis (green) are compared with those set by the CR+(MS1+MS2) analyses (purple), as well as the combination of all three analyses (blue).
	\textit{Figures were created by J. Burzynski.}
	}
	\label{fig:CompareLimits_600_1000}
\end{figure}

The CR analysis treated the lower mass $\Phi$ ($m_H$ = 125~GeV and $m_{\Phi} = $ 200~GeV) differently from the higher mass $\Phi$ ($m_{\Phi} \geq $ 400~GeV), using a Low-$E_{\mathrm{T}}$ selection for the former and a High-$E_{\mathrm{T}}$ selection for the latter~\cite{CalRatio2016}.
This scheme was devised to optimize the selection for the lower mass samples without sacrificing background discrimination for the higher mass samples.
The trigger used in the High-$E_{\mathrm{T}}$ selection was both more efficient for selecting the signal decays, and the integrated luminosity used for the High-$E_{\mathrm{T}}$ trigger was greater than that for the Low-$E_{\mathrm{T}}$ trigger (due to the late development and implementation of the Low-$E_{\mathrm{T}}$ trigger).
These factors resulted in stronger limits being set by the CR analysis for mass points with $m_{\Phi} \geq $ 400~GeV than for mass points with $m_{\Phi} \leq $ 200~GeV.
For this reason, despite the higher selection efficiency in this analysis for mass points with higher $m_{\Phi}$ (as demonstrated in Figure~\ref{subfig:cF_mS50}), the results from this analysis did not have an impact on the total combined limits for mass points with $m_{\Phi} \geq $ 400~GeV.
%\subsection{Comparison to run 1}