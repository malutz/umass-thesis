\contentsline {FrontMatter}{\MakeTextUppercase {Acknowledgments}}{v}{chapter*.2}% 
\contentsline {FrontMatter}{\MakeTextUppercase {\uppercase {Abstract}}}{vii}{chapter*.3}% 
\contentsline {FrontMatter}{\MakeTextUppercase {List of Tables}}{xii}{chapter*.5}% 
\contentsline {FrontMatter}{\MakeTextUppercase {List of Figures}}{xiv}{chapter*.6}% 
\contentsline {Chapter}{\MakeTextUppercase {Introduction}}{1}{chapter*.7}% 
\contentsline {Chapter}{\numberline {1.}\MakeTextUppercase {Theory motivation}}{4}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Standard Model}{4}{section.1.1}% 
\contentsline {section}{\numberline {1.2}BSM and the Hidden Sector}{8}{section.1.2}% 
\contentsline {subsection}{\numberline {1.2.1}The Hidden Sector}{10}{subsection.1.2.1}% 
\contentsline {subsubsection}{\numberline {1.2.1.1}Simplified HS model}{13}{subsubsection.1.2.1.1}% 
\contentsline {Chapter}{\numberline {2.}\MakeTextUppercase {The ATLAS experiment at the LHC}}{14}{chapter.2}% 
\contentsline {section}{\numberline {2.1}ATLAS}{14}{section.2.1}% 
\contentsline {subsection}{\numberline {2.1.1}Inner Detector}{15}{subsection.2.1.1}% 
\contentsline {subsection}{\numberline {2.1.2}Calorimeters}{17}{subsection.2.1.2}% 
\contentsline {subsection}{\numberline {2.1.3}Muon Spectrometer}{18}{subsection.2.1.3}% 
\contentsline {subsection}{\numberline {2.1.4}Trigger System}{20}{subsection.2.1.4}% 
\contentsline {Chapter}{\numberline {3.}\MakeTextUppercase {Samples}}{22}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Data}{22}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Simulation}{23}{section.3.2}% 
\contentsline {subsection}{\numberline {3.2.1}Generation of MC samples}{23}{subsection.3.2.1}% 
\contentsline {subsection}{\numberline {3.2.2}Signal MC samples}{24}{subsection.3.2.2}% 
\contentsline {subsection}{\numberline {3.2.3}Di-jet MC samples}{26}{subsection.3.2.3}% 
\contentsline {Chapter}{\numberline {4.}\MakeTextUppercase {Reconstruction}}{28}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Standard objects}{28}{section.4.1}% 
\contentsline {subsection}{\numberline {4.1.1}Jets}{28}{subsection.4.1.1}% 
\contentsline {subsection}{\numberline {4.1.2}Muons}{29}{subsection.4.1.2}% 
\contentsline {subsection}{\numberline {4.1.3}Standard tracks}{32}{subsection.4.1.3}% 
\contentsline {section}{\numberline {4.2}Displaced tracking}{34}{section.4.2}% 
\contentsline {section}{\numberline {4.3}Displaced vertices in the ID}{36}{section.4.3}% 
\contentsline {section}{\numberline {4.4}Displaced vertices in the MS}{37}{section.4.4}% 
\contentsline {Chapter}{\numberline {5.}\MakeTextUppercase {Selection}}{39}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Event Selection}{39}{section.5.1}% 
\contentsline {subsection}{\numberline {5.1.1}Trigger}{39}{subsection.5.1.1}% 
\contentsline {subsubsection}{\numberline {5.1.1.1}Scale factors for the muon RoI cluster trigger}{40}{subsubsection.5.1.1.1}% 
\contentsline {section}{\numberline {5.2}MS vertex selection}{41}{section.5.2}% 
\contentsline {section}{\numberline {5.3}ID vertex selection}{42}{section.5.3}% 
\contentsline {subsection}{\numberline {5.3.1}IDVX reconstruction efficiency }{53}{subsection.5.3.1}% 
\contentsline {subsection}{\numberline {5.3.2}IDVX residuals}{57}{subsection.5.3.2}% 
\contentsline {section}{\numberline {5.4}Overall event cutflow}{59}{section.5.4}% 
\contentsline {Chapter}{\numberline {6.}\MakeTextUppercase {Background}}{65}{chapter.6}% 
\contentsline {section}{\numberline {6.1}Estimation method}{65}{section.6.1}% 
\contentsline {section}{\numberline {6.2}Validation of the background estimation}{68}{section.6.2}% 
\contentsline {subsection}{\numberline {6.2.1}Jet multiplicity impact on the background estimation}{74}{subsection.6.2.1}% 
\contentsline {section}{\numberline {6.3}Expected number of background events}{81}{section.6.3}% 
\contentsline {Chapter}{\numberline {7.}\MakeTextUppercase {Systematic Uncertainties}}{82}{chapter.7}% 
\contentsline {section}{\numberline {7.1}Systematic uncertainties on displaced tracking and vertex reconstruction in the ID}{82}{section.7.1}% 
\contentsline {section}{\numberline {7.2}Uncertainty on integrated luminosity}{86}{section.7.2}% 
\contentsline {section}{\numberline {7.3}MSVX reconstruction efficiency}{86}{section.7.3}% 
\contentsline {section}{\numberline {7.4}Muon RoI cluster trigger, scale factor uncertainty}{87}{section.7.4}% 
\contentsline {section}{\numberline {7.5}Pileup uncertainty}{87}{section.7.5}% 
\contentsline {section}{\numberline {7.6}PDF uncertainty}{88}{section.7.6}% 
\contentsline {Chapter}{\numberline {8.}\MakeTextUppercase {Results}}{90}{chapter.8}% 
\contentsline {section}{\numberline {8.1}Global Efficiency}{90}{section.8.1}% 
\contentsline {subsection}{\numberline {8.1.1}Lifetime Extrapolation}{90}{subsection.8.1.1}% 
\contentsline {section}{\numberline {8.2}Observed signal region events}{93}{section.8.2}% 
\contentsline {section}{\numberline {8.3}Limits}{93}{section.8.3}% 
\contentsline {subsection}{\numberline {8.3.1}Limit setting procedure}{93}{subsection.8.3.1}% 
\contentsline {subsection}{\numberline {8.3.2}Limits from this analysis}{95}{subsection.8.3.2}% 
\contentsline {subsection}{\numberline {8.3.3}Combined limits}{98}{subsection.8.3.3}% 
\contentsline {Chapter}{\numberline {9.}\MakeTextUppercase {Conclusion}}{107}{chapter.9}% 
\setcounter {tocdepth}{0}
\contentsline {Appendix}{\numberline {A.}\MakeTextUppercase {Large Radius Tracking}}{109}{appendix.A}% 
\contentsline {Appendix}{\numberline {B.}\MakeTextUppercase {Analysis Cutflows}}{116}{appendix.B}% 
\contentsline {BackMatter}{\MakeTextUppercase {Bibliography}}{119}{appendix*.88}% 
